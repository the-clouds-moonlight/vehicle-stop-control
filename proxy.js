const commonIP = require('./commonIP');

const proxy = {
    //请求时映射反向代理
    '/api/server': {
        target: commonIP.remoteServerIP,  //反向代理指向后台ip地址
        pathRewrite: {
            '^/api/server': ''  //地址重定向清除
        },
        changeOrigin: true,  //target可以是域名
        // secure:false //反向代理可支持https
    }
}

module.exports = proxy;