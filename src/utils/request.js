import cookie from 'react-cookies';

// http 请求
export default function request(method, url, body, history) {
    method = method.toUpperCase();
    if (method === 'GET') {
        body = undefined;
    } else {
        body = body && JSON.stringify(body);
    }
    const c_token = cookie.load('x-auth-token');
    sessionStorage.setItem('token', c_token);
    const yToken = localStorage.getItem('y-auth-token');
    return fetch(url, {
        method: method,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'x-auth-token': c_token,
            'y-auth-token': yToken ? window.btoa(window.btoa(yToken)) : undefined
        },
        body: body
    }).then((res) => {
        const token = res.headers.get('x-auth-token');
        if (token) {
            cookie.save('x-auth-token', token, { path: '/' });
        }
        console.log(res.headers.get('res'), 'res')

        if (res.headers.get('res') === 'authentication') {
            sessionStorage.setItem('validation', 1);
        } else if (res.headers.get('res') === 'AuthenticationSuccessful') {
            sessionStorage.setItem('validation', 2);
            return res.json();
        } else {
            sessionStorage.removeItem('validation');
            sessionStorage.removeItem('y-auth-token');
        }
        if (res.status === 401) {
            history.push('/');
            return Promise.reject('Unauthorized.');
        } else {console.log(res)
            return res.json();
        }
    });
}

export const get = url => request('GET', url);
export const post = (url, body) => request('POST', url, body);