import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css'
import { Provider } from 'react-redux';
import store from './store';
// import { createBrowserHistory } from "history"
import { ConfigProvider } from 'antd'
import zhCN from 'antd/es/locale/zh_CN';
import * as serviceWorker from './serviceWorker';



import RouterConfig from '@router/routerConfig';


// const his = createBrowserHistory()

ReactDOM.render(
  // <React.StrictMode>
  <ConfigProvider locale={zhCN}>
    <Provider store={store}>
      <RouterConfig />
    </Provider>
  </ConfigProvider>
  // </React.StrictMode>
  ,
  document.getElementById('root')
);

// 热更新
if (module.hot) {
  module.hot.accept(err => {
    if (err) {
      console.error('module.hot，', err);
    }
  });
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
