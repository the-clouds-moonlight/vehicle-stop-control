import React, { Component } from 'react';
import { Upload, Button, Card, message, Modal } from 'antd';
import nProgress from 'nprogress'; // 引入请求进度条插件
import 'nprogress/nprogress.css'; // 请求进度条css样式
import { DownloadOutlined } from '@ant-design/icons'

import { connect } from 'react-redux';
import * as action from './store/action';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            uploadInfo: {},
        };
    }

    componentDidMount() {
        this.props.getTemplates();
    }

    uploadFile = ({ file }) => {
        if (file.status === 'done') {
            if (file.response !== undefined) {
                if (file.response.response.res) {
                    message.success('上传成功');
                    console.log(file.response.response);
                    let data = file.response.response.message;
                    this.setState({
                        visible: true,
                        uploadInfo: data,
                    });
                } else {
                    message.error(file.response.exception, 1);
                }
            }
            nProgress.done();
        } else if (file.status === 'uploading') {
            nProgress.start();
            nProgress.set(0.5);
        } else {
            message.error('excle上传失败, 请重新上传', 1);
            nProgress.done();
            return;
        }
    };

    render() {
        const uploadFileSimple = {
            name: 'file',
            accept: '.xlsx,.xls,application/vnd.ms-excel',
            showUploadList: false,
            action: '/api/server/demo/excel/readExcel',
            data: { excelHandler: 'simpleExcel' },
            onChange: this.uploadFile,
        };

        const uploadFileComplex = {
            name: 'file',
            accept: '.xlsx,.xls,application/vnd.ms-excel',
            showUploadList: false,
            data: { excelHandler: 'complexExcel' },
            action: '/api/server/demo/excel/readExcel',
            onChange: this.uploadFile,
        };

        const { uploadInfo, visible } = this.state;

        return (
            <div className="App">
                <Card>
                    <Upload {...uploadFileSimple}>
                        <Button type="primary" style={{ marginRight: '10px' }}>
                            上传不含合并单元格的Excel
                        </Button>
                    </Upload>
                    <Button
                        type="primary"
                        href={'/api/server' + this.props.templatePath.excelExampleTemplates}
                    >
                        下载模版文件
                    </Button>
                    <Button
                        style={{ left: '10px' }}
                        icon={<DownloadOutlined />}
                        type="primary"
                        onClick={() =>
                          this.props.downloadExcel({
                            tableName:"Excel报表",
                        })
                        }
                    >
                        下载Excel报表
                    </Button>
                </Card>
                <Card>
                    <Upload {...uploadFileComplex}>
                        <Button type="primary" style={{ marginRight: '10px' }}>
                            上传内含合并单元格的Excel
                        </Button>
                    </Upload>
                    <Button
                        type="primary"
                        href={'/api/server' + this.props.templatePath.excelExampleTemplates}
                    >
                        下载模版文件
                    </Button>
                </Card>
                <Modal
                    visible={visible}
                    onCancel={() => {
                        this.setState({ visible: false });
                    }}
                    footer={null}
                    zIndex={10}
                    destroyOnClose
                >
                    <div style={{ fontSize: '20px', letterSpacing: '2px' }}>
                        <span>
                            解析成功
                            <em style={{ fontSize: '30px', color: 'green' }}>
                                {uploadInfo.successDataSize}条
                            </em>
                            数据
                        </span>
                    </div>
                    <div style={{ fontSize: '20px', letterSpacing: '2px' }}>
                        <span>
                            解析失败
                            <em style={{ fontSize: '30px', color: 'red' }}>
                                {uploadInfo.failureDataSize}条
                            </em>
                            数据
                        </span>
                    </div>
                    {/* {uploadInfo.coverSize > 0 ?
            <div style={{ fontSize: "20px", letterSpacing: "2px" }}>
              <span>本次导入有<em style={{ fontSize: "30px", color: 'orange' }}>{uploadInfo.coverSize}条</em>数据与系统内数据重复</span>
              <Button style={{ border: "none", color: '#5468ff' }} onClick={() => this.setState({ isCoverVisible: true })} >&gt;&gt;&gt;查看详情</Button>
            </div> : null} */}
                    <p>是否下载失败数据!</p>
                    <Button
                        style={{ marginRight: 5 }}
                        onClick={() => {
                            this.setState({ visible: false });
                        }}
                    >
                        <a href={'/api/server' + uploadInfo.parseExcelPath}>确定</a>
                    </Button>
                    <Button
                        style={{ marginLeft: 5 }}
                        onClick={() => {
                            this.setState({ visible: false });
                        }}
                        danger
                    >
                        取消
                    </Button>
                </Modal>
            </div>
        );
    }
}

const mapStateToPorps = state => ({
    // excel模版路径
    templatePath: state.excelDemo.data,
});

const mapDispatchToProps = dispatch => ({
    // 获取表单模版路径
    getTemplates() {
        dispatch(action.getTemplates());
    },
    downloadExcel(data) {
      dispatch(action.downloadExcel(data))
    },
});

export default connect(mapStateToPorps, mapDispatchToProps)(Index);
