import * as ActionTypes from './actionTypes';
import request from '@utils/request'
import { message } from 'antd';

export const getTemplates = () => {
  return (dispatch) => {
    request('POST', "/api/server/demo/excel/getTemplates", {
      'cmd': 'getTemplates',
      'type': 'request',
      'request': {

      }
    }).then(obj => {
      const res = obj.response;
      if (res.res) {
        const message = res.message;
        // 获取模版文件
        dispatch(getTemplatesRequest(message));
      } else {
        message('获取模版文件失败', 3);
      }
    })
  }
}

export const downloadExcel = (data) =>{
  return (dispatch) => {
    request('POST', "/api/server/demo/excel/downloadExcel", {
      'cmd': 'downloadExcel',
      'type': 'request',
      'request': {
        ...data
      }
    }).then(obj => {
      const res = obj.response;
      if (res.res) {
        const message = res.message;
        // 获取模版文件
        dispatch(downloadExcelRequest(message));
        console.log(' => ' , message.filePath)
        window.location.href = '/api/server' + message.filePath;
      } else {
        message('获取模版文件失败', 3);
      }
    })
  }
}

const getTemplatesRequest = (data) =>({
  type: ActionTypes.GETTEMPLATEFILE,
  data
})
const downloadExcelRequest = (data) =>({
  type: ActionTypes.GETTEMPLATEFILE,
  data
})