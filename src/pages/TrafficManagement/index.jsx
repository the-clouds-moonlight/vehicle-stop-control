/*
 * @Author: mwxp (ljh) 
 * @Date: 2022-05-16 17:15:58 
 * @Last Modified by: mwxp (ljh)
 * @Last Modified time: 2022-05-16 22:14:06
 */
import React, { useState } from 'react'
import style from './style.less'
import { Form, Input, Button, Radio, Modal, Col, Row, Table, Descriptions } from 'antd';
import { AudioOutlined } from '@ant-design/icons';

const { Search } = Input;

const Index = () => {

    // const { loading, selectedRowKeys } = useState();
    const rowSelection = {

    };

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };


    const columns = [
        {
            title: '车牌号',
            dataIndex: 'Licenseplatenumber',
            key: 'Licenseplatenumber',
        },
        {
            title: '厂牌型号',
            dataIndex: 'Labeltype',
            key: 'Labeltype',
        },
        {
            title: '车位到期日期',
            dataIndex: 'Maturitydateofcommercialinsurance',
            key: 'Maturitydateofcommercialinsurance',
        },
        {
            title: '物业到期日期',
            dataIndex: 'Compulsoryinsuranceduedate',
            key: 'Compulsoryinsuranceduedate',
        },
        {
            title: '车位费用',
            dataIndex: 'Businessrisks',
            key: 'Businessrisks',
        },
        {
            title: '物业费用',
            dataIndex: 'insurance',
            key: 'insurance',
        },
        {
            title: '合计费用',
            dataIndex: 'Totalcost',
            key: 'Totalcost',
        },
    ]

    const data = [
        {
            key: '1',
            Licenseplatenumber: '鄂A67Q76',
            Labeltype: 'x5',
            Maturitydateofcommercialinsurance: '2024.10.12',
            Compulsoryinsuranceduedate: '2024.10.12',
            Businessrisks: '8000',
            insurance: '6000',
            Totalcost: '14000',
        },
        {
            key: '2',
            Licenseplatenumber: '鄂AB7QN6',
            Labeltype: 'x3',
            Maturitydateofcommercialinsurance: '2024.11.12',
            Compulsoryinsuranceduedate: '2024.11.12',
            Businessrisks: '8000',
            insurance: '6000',
            Totalcost: '14000',
        },
        {
            key: '3',
            Licenseplatenumber: '鄂A6JQ7L',
            Labeltype: 'x7',
            Maturitydateofcommercialinsurance: '2024.9.12',
            Compulsoryinsuranceduedate: '2024.9.12',
            Businessrisks: '8000',
            insurance: '6000',
            Totalcost: '14000',
        },
        {
            key: '4',
            Licenseplatenumber: '鄂A6TQ7I',
            Labeltype: 'x2',
            Maturitydateofcommercialinsurance: '2024.4.12',
            Compulsoryinsuranceduedate: '2024.4.12',
            Businessrisks: '8000',
            insurance: '6000',
            Totalcost: '14000',
        },
    ];

    return (
        <div>
            <Form
                name="basic"
                // labelCol={{ span: 4 }}
                // wrapperCol={{ span: 16 }}
                initialValues={{ remember: true }}
            >
                <Row gutter={20}>
                    <Col span={8}>
                        <Form.Item >
                            <Search placeholder="按车牌号或保单号" />
                        </Form.Item>
                    </Col>

                    <Col span={8}>
                        <Form.Item>
                            <Input placeholder="单位1.1" />
                        </Form.Item>
                    </Col>

                    <Col span={4}>
                        <Button type='primary'>高级查询</Button>
                        <div>
                            (模糊查询)
                        </div>
                    </Col>
                </Row>

                <Row gutter={8}>
                    <Col span={2}>
                        <Button onClick={showModal}>新增</Button>
                    </Col>
                    <Col span={2}>
                        <Button onClick={showModal}>修改</Button>
                    </Col>
                    <Col span={2}>
                        <Button>删除</Button>
                    </Col>
                    <Col span={2}>
                        <Button>数据</Button>
                    </Col>
                </Row>

            </Form>


            <div style={{ marginTop: '2vh' }}>
                <Table rowSelection={rowSelection} bordered={true} dataSource={data} columns={columns} />
            </div>

            <Modal title="详情查看" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                {/* <Radio.Group >
                    <Radio value={1}>模糊查询</Radio>
                    <Radio value={2}>精确查询</Radio>
                </Radio.Group> */}
                <Descriptions title="详细信息" layout="vertical" bordered>
                    <Descriptions.Item label="车牌号">鄂A67Q76</Descriptions.Item>
                    <Descriptions.Item label="厂牌型号">x5</Descriptions.Item>
                    <Descriptions.Item label="车位到期日期">2024.10.12</Descriptions.Item>
                    <Descriptions.Item label="物业到期日期">2024.10.12</Descriptions.Item>
                    <Descriptions.Item label="车位费用">8000</Descriptions.Item>
                    <Descriptions.Item label="物业费用">6000</Descriptions.Item>
                    <Descriptions.Item label="合计费用">14000</Descriptions.Item>
                </Descriptions>



                {/* <Form>
                    <Form.Item
                        label="车牌号"
                 
                        initialValue='鄂A67Q76'
                        name="车牌号">
                        <Input disabled={true}/>
                    </Form.Item>
                    <Form.Item
                        label="厂牌型号"
                     
                        initialValue='x5'
                        name="厂牌型号">
                        <Input disabled={true}/>
                    </Form.Item>
                    <Form.Item
                        label="车位到期日期"
               
                        initialValue='2024.10.12'
                        name="车位到期日期">
                        <Input disabled={true}/>
                    </Form.Item>
                    <Form.Item
                        label="物业到期日期"
                      
                        initialValue='2024.10.12'
                        name="物业到期日期">
                        <Input disabled={true}/>
                    </Form.Item>
                    <Form.Item
                        label="车位费用"
                      
                        initialValue='8000'
                        name="车位费用">
                        <Input disabled={true}/>
                    </Form.Item>
                    <Form.Item
                        label="物业费用"
                        
                        initialValue='6000'
                        name="物业费用">
                        <Input disabled={true}/>
                    </Form.Item>
                    <Form.Item
                        label="合计费用"
                        
                        initialValue='14000'
                        name="合计费用">
                        <Input disabled={true}/>
                    </Form.Item>
                </Form> */}
            </Modal>

        </div>
    )

}

export default Index
