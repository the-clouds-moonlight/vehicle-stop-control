import request from '@utils/request';
import { message } from 'antd';
import nProgress from 'nprogress';   // 引入请求进度条插件
import 'nprogress/nprogress.css';    // 请求进度条css样式

/**
 * 发送登录短信验证码
 * @param {JSONObject} data 
 * @returns 
 */
export const sendSms = (data) => {
  nProgress.start()
  nProgress.set(0.5);
  return () => {
    request('POST', '/api/server/web/rest/sms/sendsms', {
      type: 'request',
      cmd: 'sendsms',
      request: {
        ...data
      }
    }).then(obj => {
      let res = obj.response;
      if (res.res) {
        message.success('发送成功', 1);
      } else {
        message.error('发送失败，' + res.exception, 1);
      }
      
    }).catch(err => console.log(err));
    nProgress.done();
  }
}
