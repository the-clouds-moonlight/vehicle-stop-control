import { Form, Table, Tabs, Input, Button } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as action from './store/action'

const { TabPane } = Tabs;

class Index extends Component {

  constructor(props) {
    super(props);
  }

  onSend = (value) => {
    var data = {
      tel: value.tel,
      content: value.content
    }
    this.props.sendSms(data)
  }

  render() {
    const columns = [
      {
        title: '电话号码',
        dataIndex: 'tel',
        key: 'tel',
        width: 200
      },
      {
        title: '发送内容',
        dataIndex: 'content',
        key: 'content'
      },
      {
        title: '发送时间',
        dataIndex: 'time',
        key: 'time',
        width: 200
      },
      {
        title: '状态',
        dataIndex: 'status',
        key: 'status',
        width: 100
      }
    ]
    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab="发送短信" key="1">
          <div style={{"width": "80vw"}}>
            <Form onFinish={this.onSend}>
              <Form.Item style={{"width": '20vw'}} name="tel" label="手机号码"
                rules={
                  [
                    { required: true, message: "请输入手机号码" },
                    { pattern: /^1(3\d|4[5-9]|5[0-35-9]|6[2567]|7[0-8]|8\d|9[0-35-9])\d{8}$/, message: "请输入正确的手机号格式" }
                  ]
                }>
                <Input placeholder='请输入电话号码'></Input>
              </Form.Item>
              <Form.Item name="content" label="短信内容">
                <TextArea maxLength={70} showCount placeholder='请输入短信内容(70字以内)'></TextArea>
              </Form.Item>
              <Button htmlType='submit' type='primary' style={{"marginRight": "10px"}}>发送</Button>
              <Button htmlType='reset'>清空</Button>
            </Form>
          </div>
        </TabPane>
        <TabPane tab="历史记录" key="2">
          <Table bordered columns={columns} />
        </TabPane>
      </Tabs>
    )
  }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
  sendSms(data) {
    dispatch(action.sendSms(data))
  }
})


export default connect(mapStateToProps, mapDispatchToProps)(Index);
