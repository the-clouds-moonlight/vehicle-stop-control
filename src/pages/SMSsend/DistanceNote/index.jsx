import React, { Component } from 'react';
import { Card, Button, Modal, Form, Input } from 'antd';
import { connect } from 'react-redux';
import * as action from './store/action'
import { data } from 'jquery';

class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      telVisible: false,
      emailVisible: false,
      noteId: '',
    }
  }

  // 按钮点击事件
  ontelButtonClick = (id) => {
    this.setState({
      noteId: id,
      telVisible: true
    })
  }

  onEmailButtonClick = (id) => {
    this.setState({
      noteId: id,
      emailVisible: true
    })
  }

  // 发送短信
  sendNote = (value) => {
    const { tel, email } = value
    // 通过noteId获取自己需要发送的请求
    const notes = {
      "sendNote": () => { this.props.sendNote(tel) },
      "sendModifyPhoneNote": () => { this.props.sendModifyPhoneNote(tel) },
      "sendModifyPasswordVerificationCode": () => { this.props.sendModifyPasswordVerificationCode(tel) },
      "registeredAccountNumberSendMessage": () => { this.props.registeredAccountNumberSendMessage(tel) },
      "sendRegisterEmail": () => { this.props.sendRegisterEmail(email) },
      "sendModifyEmailVerificationCode": () => { this.props.sendModifyEmailVerificationCode(email) },
    }
    notes[this.state.noteId]()
    this.setState({
      noteId: '',
      telVisible: false,
      emailVisible: false
    })
  }

  render() {
    const { telVisible, emailVisible } = this.state;
    return (
      <Card title="发送短信测试页面">
        <div style={{ width: '60vw', display: 'flex', justifyContent: 'space-around' }}>
          <Button type='primary' onClick={() => this.ontelButtonClick("sendNote")}>发送登录验证码</Button>
          <Button type='primary' onClick={() => this.ontelButtonClick("sendModifyPhoneNote")}>发送修改手机号验证码</Button>
          <Button type='primary' onClick={() => this.ontelButtonClick("sendModifyPasswordVerificationCode")}>发送修改密码的短信验证码</Button>
          <Button type='primary' onClick={() => this.ontelButtonClick("registeredAccountNumberSendMessage")}>注册账号发送短信</Button>
          <Button type='primary' onClick={() => this.onEmailButtonClick("sendRegisterEmail")}>发送邮箱注册验证码</Button>
          <Button type='primary' onClick={() => this.onEmailButtonClick("sendModifyEmailVerificationCode")}>发送修改邮箱验证码</Button>
        </div>
        <Modal title="发送短信"
          visible={telVisible}
          onCancel={() => this.setState({ telVisible: false })}
          footer={null}
          destroyOnClose>
          <Form onFinish={this.sendNote}>
            <Form.Item name="tel" label="手机号"
              rules={
                [
                  { required: true, message: "请输入手机号" },
                  { pattern: /^1(3\d|4[5-9]|5[0-35-9]|6[2567]|7[0-8]|8\d|9[0-35-9])\d{8}$/, message: "请输入正确的手机号格式" }
                ]
              }
            >
              <Input></Input>
            </Form.Item>
            <Button htmlType='submit' >发送</Button>
            <Button onClick={() => {
              this.setState({
                telVisible: false,
                noteId: ''
              })
            }}>取消</Button>
          </Form>
        </Modal>

        <Modal title="发送邮件"
          visible={emailVisible}
          onCancel={() => this.setState({ emailVisible: false })}
          footer={null}
          destroyOnClose>
          <Form onFinish={this.sendNote}>
            <Form.Item name="email" label="QQ邮箱"
              rules={
                [
                  { required: true, message: "请输入QQ邮箱" },
                  { pattern: /^\w+([-+.]\w+)*@qq\.com$/, message: "请输入正确的QQ邮箱" }
                ]
              }
            >
              <Input></Input>
            </Form.Item>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button htmlType='submit' style={{marginRight: '10px'}} >发送</Button>
              <Button onClick={() => {
                this.setState({
                  emailVisible: false,
                  noteId: ''
                })
              }}>取消</Button>
            </div>

          </Form>
        </Modal>
      </Card>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  // 发送登录短信验证码
  sendNote(data) {
    dispatch(action.sendNote(data))
  },
  // 发送修改手机号验证码
  sendModifyPhoneNote(data) {
    dispatch(action.sendModifyPhoneNote(data))
  },
  // 发送修改密码的短信验证码
  registeredAccountNumberSendMessage(data) {
    dispatch(action.registeredAccountNumberSendMessage(data))
  },
  // 发送邮箱注册验证码
  sendRegisterEmail(data) {
    dispatch(action.sendRegisterEmail(data))
  },
  // 发送修改邮箱验证码
  sendModifyEmailVerificationCode(data) {
    dispatch(action.sendModifyEmailVerificationCode(data))
  }
})

export default connect(null, mapDispatchToProps)(Index);