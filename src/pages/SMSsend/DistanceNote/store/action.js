import request from '@utils/request';
import { message } from 'antd';
import nProgress from 'nprogress';   // 引入请求进度条插件
import 'nprogress/nprogress.css';    // 请求进度条css样式

/**
 * 发送登录短信验证码
 * @param {String} data 
 * @returns 
 */
export const sendNote = (data) => {
  nProgress.start()
  nProgress.set(0.5);
  return () => {
    request('POST', '/api/server/web/rest/note/sendNote', {
      type: 'request',
      cmd: 'sendNote',
      request: {
        "phone": data
      }
    }).then(obj => {
      let res = obj.response;
      if (res.res) {
        message.success('发送成功', 1);
      } else {
        message.error('发送失败，' + res.exception, 1);
      }
      nProgress.done();
    }).catch(err => console.log(err));
  }
}

/**
 * 发送修改手机号验证码
 * @param {String} data 
 * @returns 
 */
export const sendModifyPhoneNote = (data) => {
  nProgress.start()
  nProgress.set(0.5);
  return () => {
    request('POST', '/api/server/web/rest/note/sendModifyPhoneNote', {
      type: 'request',
      cmd: 'sendModifyPhoneNote',
      request: {
        "phone": data
      }
    }).then(obj => {
      let res = obj.response;
      if (res.res) {
        message.success('发送成功', 1);
      } else {
        message.error('发送失败，' + res.exception, 1);
      }
      nProgress.done();
    }).catch(err => console.log(err));
  }
}

/**
 * 发送修改密码的短信验证码
 * @param {String} data 
 * @returns 
 */
export const sendModifyPasswordVerificationCode = (data) => {
  nProgress.start()
  nProgress.set(0.5);
  return () => {
    request('POST', '/api/server/web/rest/note/sendModifyPasswordVerificationCode', {
      type: 'request',
      cmd: 'sendModifyPasswordVerificationCode',
      request: {
        "phone": data
      }
    }).then(obj => {
      let res = obj.response;
      if (res.res) {
        message.success('发送成功', 1);
      } else {
        message.error('发送失败，' + res.exception, 1);
      }
      nProgress.done();
    }).catch(err => console.log(err));
  }
}

/**
 * 发送修改密码的短信验证码
 * @param {String} data 
 * @returns 
 */
 export const registeredAccountNumberSendMessage = (data) => {
  nProgress.start()
  nProgress.set(0.5);
  return () => {
    request('POST', '/api/server/web/rest/note/registeredAccountNumberSendMessage', {
      type: 'request',
      cmd: 'registeredAccountNumberSendMessage',
      request: {
        "phone": data
      }
    }).then(obj => {
      let res = obj.response;
      if (res.res) {
        message.success('发送成功', 1);
      } else {
        message.error('发送失败，' + res.exception, 1);
      }
      nProgress.done();
    }).catch(err => console.log(err));
  }
}

/**
 * 发送邮箱注册验证码
 * @param {String} data 
 * @returns 
 */
 export const sendRegisterEmail = (data) => {
  nProgress.start()
  nProgress.set(0.5);
  return () => {
    request('POST', '/api/server/web/rest/note/sendRegisterEmail', {
      type: 'request',
      cmd: 'sendRegisterEmail',
      request: {
        "email": data
      }
    }).then(obj => {
      let res = obj.response;
      if (res.res) {
        message.success('发送成功', 1);
      } else {
        message.error('发送失败，' + res.exception, 1);
      }
      nProgress.done()
    }).catch(err => console.log(err));
  }
}

/**
 * 发送修改邮箱验证码
 * @param {String} data 
 * @returns 
 */
 export const sendModifyEmailVerificationCode = (data) => {
  nProgress.start()
  nProgress.set(0.5);
  return () => {
    request('POST', '/api/server/web/rest/note/sendModifyEmailVerificationCode', {
      type: 'request',
      cmd: 'sendModifyEmailVerificationCode',
      request: {
        "email": data
      }
    }).then(obj => {
      let res = obj.response;
      if (res.res) {
        message.success('发送成功', 1);
      } else {
        message.error('发送失败，' + res.exception, 1);
      }
      nProgress.done()
    }).catch(err => console.log(err));
  }
}