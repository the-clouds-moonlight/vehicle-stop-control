import React, { Component } from 'react'
import { Card } from 'antd'
import RoleTable from './RoleTable.jsx'
import { connect } from 'react-redux'
import * as actions from './store/action'
class RoleConfig extends Component {

    componentDidMount() {
        this.props.getRoles();
    }

    render() {
        const { data,bindData } = this.props;

        return (
            <Card>
                <RoleTable
                    data={data}
                    bindData={bindData}
                    createRole={this.createRole}
                    changeRole={this.changeRole}
                    deleteRole={this.deleteRole}
                    bindRouter={this.bindRouter}
                    getBindRoute={this.getBindRoute}
                />
            </Card>
        )
    }



    deleteRole = (key) => {
        console.log('删除信息', key)
        this.props.deleteRoles(key)
    }

    createRole = (value) => {
        console.log('添加信息', value)
        console.log(JSON.stringify(value))
        this.props.createRoles(value)
    }

    changeRole = (value) => {
        console.log('修改信息', value)
        this.props.changeRoles(value)
    }

    getBindRoute = (id) => {
        console.log('查找角色已绑定的路由', id)
        this.props.getBindRoutes(id)
    }

    bindRouter = (value) => {
        console.log('绑定路由', value)
        this.props.bindRouters(value)
    }


}

const mapStateToProps = (state) => ({
    data: state.roleConfig.data,
    bindData: state.roleConfig.bindData
})

const mapDispatchToProps = (dispatch) => ({
    getRoles() {
        dispatch(actions.getRoleRequest())
    },
    createRoles(value) {
        dispatch(actions.createRoleRequest(value))
    },
    changeRoles(value) {
        dispatch(actions.changeRoleRequest(value))
    },
    deleteRoles(id) {
        dispatch(actions.deleteRoleRequest(id))
    },
    getBindRoutes(id) {
        dispatch(actions.getBindRouterRequest(id))
    },
    bindRouters(value) {
        dispatch(actions.bindRouterRequest(value))
    }

})

export default connect(mapStateToProps, mapDispatchToProps)(RoleConfig);
