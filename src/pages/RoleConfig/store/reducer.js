import * as actionType from './actionType';

export default (state = {}, action) => {
    switch (action.type) {
        case actionType.ROLE_GET_ROUTE: {
            return { ...state, data: action.data }
        }
        case actionType.BINDROUTERCONFIG_GET_ROUTE: {
            return { ...state, bindData: action.data }
        }
        default:
            return state;
    }
}