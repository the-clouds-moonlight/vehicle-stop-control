export const ROLE_GET_ROUTE = 'roleConfig/getRoute';
export const ROLE_CREATE_ROUTE = 'roleConfig/createRoute';
export const ROLE_CHANGE_ROUTE = 'roleConfig/changeRoute';
export const ROLE_DELETE_ROUTE = 'roleConfig/deleteRoute';
export const ROUTECONFIG_GET_ROUTE = 'routerConfig/getRoute';
export const BINDROUTERCONFIG_GET_ROUTE = 'roleConfig/getBindRouter';