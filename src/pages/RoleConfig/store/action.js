import request from '@utils/request';
import * as actionTypes from './actionType'
import { message } from 'antd';

//获取动态角色
export const getRoleRequest = () => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/RoleRest/getAllRole', {
            cmd: 'getAllRole',
            type: 'request',
            request: {
 
            }
        }).then(obj => {
            console.log(obj, 'roles')
            let res = obj.response;
            if (res.res) {
                message.success('获取角色信息成功', 1);
                console.log('res.message => ' , res.message);
                let k = 1;
                res.message = res.message.map((obj) => {
                    obj.id = k;
                    k ++;
                    return obj;
                })
                dispatch(getRoleReducer(res.message))
            } else {
                message.error('获取角色信息失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

const getRoleReducer = (data) => ({
    type: actionTypes.ROLE_GET_ROUTE,
    data
})

const getBindRouterReducer = (data) => ({
    type: actionTypes.BINDROUTERCONFIG_GET_ROUTE,
    data:data
})

//创建角色
export const createRoleRequest = (value) => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/RoleRest/addRoleEntity', {
            cmd: 'addRoleEntity',
            type: 'request',
            request: {
                ...value
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('创建角色成功', 1);
                dispatch(getRoleRequest())
            } else {
                message.error('创建角色失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

export const changeRoleRequest = (value) => {
    return (dispatch) => {
        console.log(value)
        request('POST', '/api/server/web/rest/RoleRest/updateRole', {
            cmd: 'updateRole',
            type: 'request',
            request: {
                ...value
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('修改角色成功', 1);
                dispatch(getRoleRequest())
            } else {
                message.error('修改角色失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

export const deleteRoleRequest = (id) => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/RoleRest/deleteRole', {
            cmd: 'deleteRole',
            type: 'request',
            request: {
                id
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('删除角色成功', 1);
                dispatch(getRoleRequest())
            } else {
                message.error('删除角色失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}


export const getRouterRequest = () => {
    return (dispatch) => {
        request('POST', '/api/server/web/rest/router/findRouter', {
            cmd: 'findRouter',
            type: 'request',
            request: {

            }
        }).then(obj => {
            console.log(obj, 'routes')
            let res = obj.response;
            if (res.res) {
                // message.success('获取路由成功', 1);
                dispatch(getRouterReducer(res.message))
            } else {
                message.error('获取路由失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

const getRouterReducer = (data) => ({
    type: actionTypes.ROUTECONFIG_GET_ROUTE,
    data
})

// 根据角色查找路由
export const getBindRouterRequest = (id) => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/RoleRest/getBindRouter', {
            cmd: 'getBindRouter',
            type: 'request',
            request: {
                id
            }
        }).then(obj => {
            let res = obj.response;
        
            console.log(obj,'bindroute')
            if (res.res) {
                message.success('角色查找路由成功', 1);
                dispatch((getBindRouterReducer(res.message)))
            } else {
                message.error('角色查找路由失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

//角色与路由进行绑定
export const bindRouterRequest = (value) => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/RoleRest/bindRouters', {
            cmd: 'bindRouters',
            type: 'request',
            request: {
                ...value
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('角色绑定路由成功', 1);
                dispatch(getRouterRequest())
            } else {
                message.error('角色绑定路由失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}



