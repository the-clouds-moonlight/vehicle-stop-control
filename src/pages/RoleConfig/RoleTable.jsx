import React, { Component, Fragment } from 'react';
import { Space, Button, Table, Input, Drawer, Modal, Form, Typography } from 'antd';
import {
    EditOutlined,
    DeleteOutlined
} from '@ant-design/icons';
import DrawTree from './DrawTree.jsx';


export default class RoleTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            codeVisible: false,
            modalVisible: false,
            actionType: 1,
            renderData: {},
            actionKey: undefined,

            routerList: [],
            id: undefined,
        }
    }


    render() {

        const { codeVisible, modalVisible, actionType, renderData } = this.state;
        const { data, bindData } = this.props;

        const columns = [
            {
                title: '序号',
                dataIndex: 'id',
                key: 'id',
                align: 'center'
            },
            {
                title: '角色名称',
                dataIndex: 'roleName',
                key: 'roleName',
                align: 'center'
            },
            {
                title: '备注',
                dataIndex: 'remark',
                key: 'remark',
                align: 'center',
                render: () => {
                    return <span>暂无备注</span>
                },
                width: 400
            },
            {
                title: '操作',
                dataIndex: 'handle',
                key: 'handle',
                align: 'center',
                render: (text, record) => {
                    console.log("<<<<<<>>>>>>" + record);
                    if(record.roleName == "admin"){
                        return (
                            <Space>
                                <Button type='primary' ghost onClick={() => this.getBindRoute(record.key)}>配置路由</Button>
                                <Button type='primary' ghost icon={<EditOutlined />} onClick={() => this.setState({ actionType: 2, modalVisible: true, renderData: record, actionKey: record.key })} disabled>修改</Button>
                                <Button type='primary' ghost danger icon={<DeleteOutlined />} onClick={() => this.deleteConfirm(record.key, record.roleName)} disabled>删除</Button>
                            </Space>
                        )
                    }else{
                    return (
                        <Space>
                            <Button type='primary' ghost onClick={() => this.getBindRoute(record.key)}>配置路由</Button>
                            <Button type='primary' ghost icon={<EditOutlined />} style={{ borderColor: 'green', color: 'green' }} onClick={() => this.setState({ actionType: 2, modalVisible: true, renderData: record, actionKey: record.key })}>修改</Button>
                            <Button type='primary' ghost danger icon={<DeleteOutlined />} onClick={() => this.deleteConfirm(record.key, record.roleName)}>删除</Button>
                        </Space>
                    )
                    }
                }
            }
        ];

        return (
            <Fragment>
                <Space>
                    <Button type="primary" ghost onClick={() => this.setState({ actionType: 1, modalVisible: true })}>添加</Button>
                </Space>
                <Table columns={columns} dataSource={data}></Table>
                <Drawer
                    title='路由边栏'
                    placement='right'
                    width={'30%'}
                    closable    //是否显示右上角的关闭按钮
                    onClose={() => this.setState({ codeVisible: false })}
                    visible={codeVisible}   //Drawer 是否可见	
                    destroyOnClose      //关闭时是否删除Drawer元素

                >

                    <DrawTree onCheck={(value) => this.setState({ routerList: value })} bindData={bindData} />
                    <Space style={{ float: 'right' }}>
                        <Button type='primary' ghost onClick={() => this.onbindRouter()} >提交</Button>
                        <Button type='primary' ghost onClick={() => this.setState({ codeVisible: false })} danger>取消</Button>
                    </Space>
                </Drawer>

                <Modal
                    title={actionType === 1 ? '添加信息' : '修改信息'}
                    visible={modalVisible}
                    destroyOnClose
                    centered    //垂直居中展示 Modal
                    // closable    //是否显示右上角的关闭按钮
                    onCancel={() => this.setState({ modalVisible: false })}
                    footer={null}   //取消底部的按钮
                >
                    {
                        actionType === 1 ? (
                            <Form
                                onFinish={this.onFormCreateOk}
                            >
                                <Form.Item
                                    label='角色名称'
                                    name={['roleName']}
                                    rules={[
                                        { required: true, message: '填写角色名称' }
                                    ]}
                                >
                                    <Input placeholder='填写角色名称' />
                                </Form.Item>
                                {/* <Form.Item
                                    label='备注'
                                    name={['remark']}
                                    rules={[
                                        { required: true, message: '填写备注' }
                                    ]}
                                >
                                    <Input placeholder='填写备注' />
                                </Form.Item> */}
                                <Form.Item>
                                    <Space style={{ float: 'right' }}>
                                        <Button type='primary' ghost htmlType='submit'>提交</Button>
                                        <Button type='primary' ghost htmlType='button' onClick={() => this.setState({ modalVisible: false })} danger>取消</Button>
                                    </Space>
                                </Form.Item>
                            </Form>

                        ) : (
                            <Form
                                onFinish={this.onFormChangeOk}
                                fields={
                                    [
                                        { name: ['roleName'], value: renderData.roleName },
                                        { name: ['remark'], value: renderData.remark },
                                    ]
                                }
                            >
                                <Form.Item
                                    label='角色名称'
                                    name={['roleName']}
                                    rules={[
                                        { required: true, message: '填写角色名称' }
                                    ]}
                                >
                                    <Input placeholder='填写角色名称' />
                                </Form.Item>
                                {/* <Form.Item
                                        label='备注'
                                        name={['remark']}
                                        rules={[
                                            { required: true, message: '填写备注' }
                                        ]}
                                    >
                                        <Input placeholder='填写备注' />
                                    </Form.Item> */}
                                <Form.Item>
                                    <Space style={{ float: 'right' }}>
                                        <Button type='primary' ghost htmlType='submit'>提交</Button>
                                        <Button type='primary' ghost htmlType='button' onClick={() => this.setState({ modalVisible: false })} danger>取消</Button>
                                    </Space>
                                </Form.Item>
                            </Form>
                        )
                    }
                </Modal>
            </Fragment>
        );
    }

    deleteConfirm(key, text) {
        Modal.confirm({
            title: '确认',
            content: (
                <div>
                    是否删除
                    <Typography.Text strong type="danger">
                        {text}
                    </Typography.Text>
                        角色的信息
                </div>
            ),
            okText: '确认',
            okType: 'danger',
            cancelText: '取消',
            onOk: () => {
                console.log('key => ', key);
                this.props.deleteRole(key)
            },
            onCancel: () => {

            }
        })
    }

    onFormCreateOk = (value) => {
        this.setState({
            modalVisible: false
        })
        this.props.createRole(value)
    }

    onFormChangeOk = (value) => {
        const { renderData } = this.state;
        this.setState({
            modalVisible: false
        })
        const value2 = {
            ...value,
            id: renderData.key
        }
        this.props.changeRole(value2)
    }

    onbindRouter = () => {
        const { id, routerList } = this.state;
        let arr = [];
        for (var i = 0, len = routerList.length; i < len; i++) {
            arr.push({id: routerList[i]})
        }
        const value = {
            ids: id,
            routerList: arr
        }
        this.props.bindRouter(value)
        this.setState({ codeVisible: false })
    }


    getBindRoute = (id) => {
        this.setState({ codeVisible: true, id: id })
        console.log("<<<<<<" + id);
        this.props.getBindRoute(id)
    }

}
