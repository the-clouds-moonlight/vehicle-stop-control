import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getRouterRequest } from '../RouteConfig/store/action'
import { Tree } from 'antd';

class DrawTree extends Component {

    state = {
        checkKes: []
    }

    componentDidMount() {
        this.props.getRoutes();

    }

    componentDidUpdate(nextProps) {
        if (this.props.bindData !== nextProps.bindData) {
            this.setState({
                checkKes: this.props.bindData
            })
        }
    }

    render() {
        const { data } = this.props;
        console.log(this.state.checkKes, 'checkkey')


        return (
            <Tree
                checkable
                onCheck={this.onTreeCheck}
                checkedKeys={this.state.checkKes}
            >
                {this.renderTree(data)}
            </Tree>
        );
    }

    renderTree = (array = []) => {
        return array.map(i => {
            if (i.children) {
                return (
                    <Tree.TreeNode key={i.key} title={i.path}>
                        {this.renderTree(i.children)}
                    </Tree.TreeNode>
                )
            }
            return <Tree.TreeNode key={i.key} title={i.path} />
        })
    }

    onTreeCheck = (value) => {
        this.setState({
            checkKes: value
        }, () => this.props.onCheck(value))
        console.log(value, 'check')
    }

}

const mapStateToProps = (state) => ({
    data: state.routerConfig.data
})

const mapDispatchToProps = (dispatch) => ({
    getRoutes() {
        dispatch(getRouterRequest())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(DrawTree);
