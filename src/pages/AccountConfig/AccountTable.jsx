import React, { Component, Fragment } from 'react';
import { Space, Button, Table, Modal, Typography, Form, Input, Select } from 'antd'
import {
    EditOutlined,
    DeleteOutlined
} from '@ant-design/icons';
import { useForm } from 'antd/lib/form/Form';

class AccountTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            codeVisible: false,
            modalVisible: false,  //  Model隐藏
            actionType: 1,      //   1‘新建 2’修改
            renderData: {},     //   返回的数据
            actionKey: undefined,    //  数据
            ids:[]
        }
    }

    // onSelectChange =(e) =>{
    //     this.state({
    //         ids:e
    //     })
    // }
    render() {
        const { modalVisible, actionType, renderData } = this.state;
        const { data } = this.props;        //  用户数据
        const { role } = this.props;

        //   // 全选按钮
        //   const { ids } = this.state;
        //   const rowSelection = {
        //       ids,
        //       onChange: this.onSelectChange,
        //       selections: [
        //           Table.SELECTION_ALL,
        //           Table.SELECTION_INVERT,
        //           Table.SELECTION_NONE,
        //       ]
        //   }

        const columns = [
            {
                title: '序号',
                dataIndex: 'key',
                key: 'key',
                align: 'center',
            },
            {
                title: '账号',
                dataIndex: 'account',
                key: 'account',
                align: 'center',
            },
            {
                title: '密码',
                dataIndex: 'password',
                key: 'password',
                align: 'center',
            },
            {
                title: '角色',
                dataIndex: 'roleName',
                key: 'roleName',
                align: 'center',
            },
            // {
            //     title: '备注',
            //     dataIndex: 'remarks',
            //     key: 'remarks',
            //     align: 'center',
            // },
            {
                title: '操作',
                dataIndex: 'operation',
                key: 'operation',
                align: 'center',
                render: (text, record) => {
                    console.log(">>>><<<<" + record.roleName);
                   if(record.roleName == "admin"){
                    return (
                        <Space>
                            <Button type='primary' icon={<EditOutlined />} onClick={() => this.setState({ actionType: 2, modalVisible: true, renderData: record, actionKey: record.id,roleId: record.roleId })} disabled>修改</Button>
                            <Button type='primary' icon={<DeleteOutlined />} ghost danger onClick={() => this.deleteConfirm(record.key, record.account)} disabled >删除</Button>
                        </Space>
                    )
                   }else{
                    return (
                        <Space>
                            <Button type='primary' icon={<EditOutlined />} ghost style={{ borderColor: 'green', color: 'green' }} onClick={() => this.setState({ actionType: 2, modalVisible: true, renderData: record, actionKey: record.id,roleId: record.roleId })} >修改</Button>
                            <Button type='primary' icon={<DeleteOutlined />} ghost danger onClick={() => this.deleteConfirm(record.key, record.account)} >删除</Button>
                        </Space>
                    )
                   }
                }

            },
        ];
        return (
            <Fragment>
                {/* {console.log("data的值=======》》》》》》", data.account)} */}
                <Button type='primary' ghost onClick={() => this.setState({ actionType: 1, modalVisible: true })}>添加</Button>
                <Table columns={columns} dataSource={data} style={{ marginTop: 10 }} />
                <Modal
                    title={actionType === 1 ? '添加用户' : '修改信息'}
                    visible={modalVisible}
                    destroyOnClose
                    centered
                    onCancel={() => this.setState({ modalVisible: false })}
                    footer={null}
                >
                    {
                        actionType === 1 ? (
                            <Form
                                onFinish={this.onFormCreateOk}
                            // fields={
                            //     [
                            //         { name: ['strict'], value: false },
                            //         { name: ['exact'], value: false }
                            //     ]
                            // }
                            >
                                <Form.Item
                                    label='账号'
                                    name={['account']}
                                    rules={[{ required: true, message: '请输入账号' }]}
                                >
                                    <Input placeholder='请输入账号' />
                                </Form.Item>
                                <Form.Item
                                    label='密码'
                                    name={['password']}
                                    rules={[{ required: true, message: '请输入密码' }]}
                                >
                                    <Input.Password placeholder='请输入密码' />
                                </Form.Item>
                                {/* <Form.Item
                                    label='备注'
                                    name={['remarks']}
                                    rules={[{ required: true, message: '请输入备注' }]}
                                >
                                    <Input placeholder='请输入备注' />
                                </Form.Item> */}
                                <Form.Item
                                    label='角色'
                                    name={['roleId']}
                                    rules={[{ required: true, message: '请输入角色' }]}
                                >
                                    <Select placeholder='请输入角色' onSelect={this.addSelectOption}>
                                        {
                                            role.map(i => {
                                                if(i.roleName != 'admin'){
                                                return (
                                                    <Select.Option key={i.id} >{i.roleName}</Select.Option>
                                                )
                                                }
                                            })
                                        }
                                    </Select>
                                </Form.Item>
                                <Form.Item>
                                    <Space style={{ float: 'right' }}>
                                        <Button type='primary' ghost htmlType='submit'>提交</Button>
                                        <Button type='primary' ghost htmlType='button' onClick={() => this.setState({ modalVisible: false })} danger>取消</Button>
                                    </Space>
                                </Form.Item>
                            </Form>
                        ) : (
                                <Form
                                    onFinish={this.onFormChangeOk}
                                    fields={
                                        [
                                            { name: ['account'], value: renderData.account },
                                            { name: ['password'], value: renderData.password },
                                            { name: ['remarks'], value: renderData.remarks },
                                            { name: ['roleId'], value: renderData.roleId },
                                        ]
                                    }
                                >

                                    <Form.Item
                                    label= '账号'
                                    name ={['account']}
                                    rules = {[{required: true, message:'请输入账号'}]}>
                                        <Input placeholder='请输入账号' />
                                    </Form.Item>
                                    <Form.Item
                                        label='密码'
                                        name={['password']}
                                        rules={[{ required: true, message: '请输入密码' }]}
                                    >
                                        <Input.Password placeholder='请输入密码' />
                                    </Form.Item>
                                    {/* <Form.Item
                                        label='备注'
                                        name={['remarks']}
                                        rules={[{ required: true, message: '请输入备注' }]}
                                    >
                                        <Input placeholder='请输入备注' />
                                    </Form.Item> */}
                                    <Form.Item
                                        label='角色'
                                        name={['roleId']}
                                        rules={[{ required: true, message: '请输入角色' }]}
                                    >
                                        <Select placeholder='请输入角色'>
                                            {
                                                role.map(i => {
                                                    if(i.roleName != "admin"){
                                                    return (
                                                        <Select.Option key={i.id} id={i.id} value={i.id}>{i.roleName}</Select.Option>
                                                    )
                                                    }
                                                })
                                            }
                                        </Select>
                                    </Form.Item>
                                    <Form.Item>
                                        <Space style={{ float: 'right' }}>
                                            <Button type='primary' ghost htmlType='submit'>提交</Button>
                                            <Button type='primary' ghost htmlType='button' onClick={() => this.setState({ modalVisible: false })} danger>取消</Button>
                                        </Space>
                                    </Form.Item>
                                </Form>
                            )

                    }

                </Modal>
            </Fragment>
        );
    }

    //修改
    onFormChangeOk = (value) => {
        const { renderData } = this.state;
        this.setState({
            modalVisible: false
        })
        const value2 = {
            ...value,
            id: renderData.id
        }
        this.props.changeRoute(value2)
    }
    //添加
    onFormCreateOk = (value) => {
        this.setState({
            modalVisible: false
        })
        this.props.createRoute(value)
    }
    //删除警告弹窗
    deleteConfirm(id, text) {
        Modal.confirm({
            title: '确认',
            content: (
                <div>
                    是否删除该用户:
                    <Typography.Text strong type="danger">
                        {text}
                    </Typography.Text>
                            的信息
                </div>
            ),
            okText: '确认',
            okType: 'danger',
            cancelText: '取消',
            onOk: () => {
                id = id
                this.props.deleteRoute(id)
            },
            onCancel: () => {

            }
        })
    };

    //  添加选择角色
    addSelectOption = e => {
        console.log(typeof Number(e))
        if(e != null){
        this.setState({
            addSelectOptionId: Number(e)
        })
    }else{
        this.setState({
            addSelectOptionId: this.state.roleId
        })
    }
    console.log("111111111",this.state.addSelectOptionId)
    }
}

export default AccountTable;