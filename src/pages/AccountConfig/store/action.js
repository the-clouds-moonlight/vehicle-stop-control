import request from '@utils/request';
import * as actionTypes from './actionType'
import { message } from 'antd';

//查找用户
export const getBasicDataRequest = () => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/basicUserRest/getBasicUser', {
            cmd: 'getBasicUser',
            type: 'request',
            request: {

            }
        }).then(obj => {
            console.log(obj, 'routes')
            let res = obj.response;
            if (res.res) {
                message.success('获取用户成功', 1);
                console.log("res.message", res.message);
                dispatch(getBasicDataReducer(res.message))
            } else {
                message.error('获取用户失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

const getBasicDataReducer = (data) => ({
    type: actionTypes.ROUTECONFIG_GET_ROUTE,
    data
})

//获取角色列表
export const getRole = () => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/RoleRest/findRole', {
            cmd: 'findRole',
            type: 'request',
            request: {

            }
        }).then(obj => {
            console.log(obj, 'role')
            let res = obj.response;
            if (res.res) {
                message.success('获取角色成功', 1);
                dispatch(getRoleReducer(res.message))
            } else {
                message.error('获取角色失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}
const getRoleReducer = (role) => ({
    type: actionTypes.ROUTECONFIG_GET_ROLE,
    role
})

//添加用户
export const createBasicDataRequest = (value) => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/basicUserRest/addUser', {
            cmd: 'addUser',
            type: 'request',
            request: {
                ...value
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('创建用户成功', 1);
                dispatch(getBasicDataRequest())
            } else {
                message.error('创建用户失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

// 修改用户信息
export const changeBasicDataRequest = (value) => {
    return (dispatch) => {
        console.log(value)
        request('POST', '/api/server/web/rest/basicUserRest/updateBasicUser', {
            cmd: 'updateBasicUser',
            type: 'request',
            request: {
                ...value
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('修改用户成功', 1);
                dispatch(getBasicDataRequest())
            } else {
                message.error(res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

//删除用户
export const deleteBasicDataRequest = (id) => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/basicUserRest/deleteBasicUser', {
            cmd: 'deleteBasicUser',
            type: 'request',
            request: {
                id:id
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('删除用户成功', 1);
                dispatch(getBasicDataRequest())
            } else {
                message.error(res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}


