import React, { Component } from 'react';
import AccountTable from './AccountTable';
import { Card } from 'antd';
import { connect } from 'react-redux';
import * as actions from './store/action';

class AccountConfig extends Component {

    componentDidMount() {
        this.props.getRoutes();
        this.props.getRole();
    }

    render() {

        const { data } = this.props;
        const { role } = this.props;

        return (
            <Card>
                <AccountTable 
                    data={data}
                    role={role}
                    deleteRoute={this.deleteBasicData}
                    createRoute={this.createBasicData}
                    changeRoute={this.changeBasicData}
                />
            </Card>
        );
    }
    deleteBasicData = (idList) => {
        console.log('删除数据', idList)
        this.props.deleteBasicDatas(idList)
    }
    createBasicData = (value) =>{
        console.log('添加数据'+value);
        console.log(JSON.stringify(value))
        this.props.createBasicDatas(value)
    }
    changeBasicData = (value)=>{
        console.log('修改数据', value);
        this.props.changeBasicDatas(value)
    }
}

const mapStateToProps = (state) => ({
    data: state.routerConfig.data,
    role: state.accountConfig.role
})

const mapDispatchToProps = (dispatch) => ({
    getRole(){
        dispatch(actions.getRole())
    },
    getRoutes() {
        dispatch(actions.getBasicDataRequest())
    },
    createBasicDatas(value) {
        dispatch(actions.createBasicDataRequest(value))
    },
    changeBasicDatas(value) {
        dispatch(actions.changeBasicDataRequest(value))
    },
    deleteBasicDatas(idList) {
        dispatch(actions.deleteBasicDataRequest(idList))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountConfig);