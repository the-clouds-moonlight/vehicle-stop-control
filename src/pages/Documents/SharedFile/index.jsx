import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Dropdown, Table, Menu } from 'antd';
import genduo from '@assets/genduo.svg';

export class index extends Component {


  render() {

    const menu = (
      <Menu>
        <Menu.Item>
          <span>下载</span>
        </Menu.Item>
        <Menu.Item>
          <span>详情</span>
        </Menu.Item>
      </Menu>
    );

    const columns = [
      {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
        // align: 'center'
      },
      {
        title: '所属者',
        dataIndex: 'sharePerson'
      },
      {
        title: '共享时间',
        dataIndex: 'shareTime'
      },
      {
        title: '下载次数',
        dataIndex: 'downloadCount'
      },
      {
        title: '操作',
        align: 'center',
        width: 50,
        render: () => {
          return (
            <Dropdown overlay={menu} trigger={['click']}>
              <img style={{ width: '80px', height: '20px', cursor: 'pointer' }} src={genduo} />
            </Dropdown>
          )
        }
      }
    ]

    const datasource = [
      {
        key: 1,
        name: '文件夹',
        shareTime: '2022-01-01',
        sharePerson: '管理员'
      },
      {
        key: 2,
        name: '文件夹',
        shareTime: '2022-01-01',
        sharePerson: '管理员'
      }
    ];
    return (
      <Card title="共享文档">
        <Table columns={columns} dataSource={datasource} />
      </Card>
    )
  }
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(index)