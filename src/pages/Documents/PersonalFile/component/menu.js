import React from 'react';
import { Menu } from 'antd';

// 下载菜单
export const DownloadMenu = ({tableHandle, text}) => (<Menu.Item onClick={() => tableHandle('download', text)}><span>下载</span></Menu.Item>);

// 重命名菜单
export const RenameMenu = ({tableHandle, text}) => (<Menu.Item onClick={() => tableHandle('rename', text)}><span>重新命名</span></Menu.Item>);

// 删除菜单
export const DeleteMenu = ({tableHandle, text}) => (<Menu.Item onClick={() => tableHandle('delete', text)} ><span>删除</span></Menu.Item>)