import * as ActionTypes from './actionTypes';

const reducer = (state = {}, action) => {
  switch (action.type) {
    case ActionTypes.GETFOLDERANDFILE: {
      return {
        ...state, data: action.data
      }
    }
    case ActionTypes.GETFOLDERANDFILEBYFOLDER: {
      return {
        ...state, data: action.data
      }
    }
    default: return state;
  }
}

export default reducer;