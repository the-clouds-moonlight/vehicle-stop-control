import * as ActionTypes from './actionTypes';
import request from '@utils/request'
import { message } from 'antd';

/**
 * 获取根目录
 * @returns 
 */
export const getRootFolder = () => {
  return (dispatch) => {
    request('POST', '/api/server/web/rest/DocumentRest/getRootFolder', {
      cmd: "getRootFolder",
      type: "request",
      request: {

      }
    }).then(obj => {
      const res = obj.response;
      if (res.res) {
        const message = res.message;
        // 获取模版文件
        dispatch(getRootFolderRequest(message));
      } else {
        message.error('获取根目录失败', 3);
      }
    }).catch(err => console.log(err))
  }
}

const getRootFolderRequest = (data) => ({
  type: ActionTypes.GETFOLDERANDFILE,
  data
})

/**
 * 新建文件夹
 * @param {JSONObject} data 
 * @returns 
 */
export const addFolder = (data) => {
  return (dispatch) => {
    request('POST', '/api/server/web/rest/DocumentRest/addFolder', {
      cmd: "addFolder",
      type: "request",
      request: {
        ...data
      }
    }).then(obj => {
      const res = obj.response;
      if (res.res) {
        message.success('文件夹新建成功', 1);
        if (data.folderId !== 0) {
          dispatch(listFolderAndFile(data.folderId))
        } else {
          // 获取文件根目录文件
          dispatch(getRootFolder())
        }
      } else {
        message.error(res.exception, 1);
      }
    }).catch(err => console.log(err))
  }
}

/**
 * 删除文件夹
 * @param {number} data 
 * @returns 
 */
export const deleteFolder = (data) => {
  return (dispatch) => {
    request('POST', '/api/server/web/rest/DocumentRest/deleteFolder', {
      cmd: "deleteFolder",
      type: "request",
      request: {
        folderId: data.folderId
      }
    }).then(obj => {
      const res = obj.response;
      if (res.res) {
        message.success('文件夹删除成功');
        if (data.parentFolderId == 0) {
          dispatch(getRootFolder())
        } else {
          dispatch(listFolderAndFile(data.parentFolderId))
        }
      } else {
        message.error(res.exception);
      }
    }).catch(err => console.log(err))
  }
}

/**
 * 删除文件
 * @param {number} data 
 * @returns 
 */
 export const deleteFile = (data) => {
  return (dispatch) => {
    request('POST', '/api/server/web/rest/DocumentRest/deleteFile', {
      cmd: "deleteFile",
      type: "request",
      request: {
        fileId: data.fileId
      }
    }).then(obj => {
      const res = obj.response;
      if (res.res) {
        message.success('文件删除成功');
        if (data.parentFolderId == 0) {
          dispatch(getRootFolder())
        } else {
          dispatch(listFolderAndFile(data.parentFolderId))
        }
        
      } else {
        message.error(res.exception);
      }
    }).catch(err => console.log(err))
  }
}

/**
 * 获取文件夹下的目录和文件
 * @param {number} data 
 * @returns 
 */
export const listFolderAndFile = (data) => {
  return (dispatch) => {
    request('POST', '/api/server/web/rest/DocumentRest/getFolderAndFileByFolder', {
      cmd: "getFolderAndFileByFolder",
      type: "request",
      request: {
        folderId: data
      }
    }).then(obj => {
      const res = obj.response;
      if (res.res) {
        var message = res.message;
        dispatch(listFolderAndFileRequest(message));
      } else {
        message.error(res.exception);
      }
    }).catch(err => console.log(err))
  }
}

const listFolderAndFileRequest = (data) => ({
  type: ActionTypes.GETFOLDERANDFILEBYFOLDER,
  data
})

/**
 * 修改文件夹
 * @param {JSONObject} data 
 * @returns 
 */
 export const modifyFolder = (data) => {
  return (dispatch) => {
    request('POST', '/api/server/web/rest/DocumentRest/modifyFolder', {
      cmd: "modifyFolder",
      type: "request",
      request: {
        ...data
      }
    }).then(obj => {
      const res = obj.response;
      if (res.res) {
        message.success('文件夹修改成功');
        if (data.parentFolderId !== 0) {
          dispatch(listFolderAndFile(data.parentFolderId))
        } else {
          // 获取文件根目录文件
          dispatch(getRootFolder())
        }
      } else {
        message.error(res.exception, 1);
      }
    }).catch(err => console.log(err))
  }
}