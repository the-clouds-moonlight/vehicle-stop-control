/*
 * @Author: wuxiangyi
 * @Date: 2022-03-16 09:39:55
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-03-17 21:13:30
 * @FilePath: /ReactMiddelware/src/pages/OfflineMap/index.jsx
 * @Description: 
 * 
 */
import React, { Component } from 'react'
import { Card, Input, Tabs } from 'antd'
import './index.css'
import Map from './Map'
import { connect } from 'react-redux'

const { TabPane } = Tabs;
class index extends Component {
    // tabs改变回调
    callback = (event) => {
        console.log('event => ', event);
    }

    /**
     * 获取两经纬度之间的距离
     * @param {number} e1 点1的东经, 单位:角度, 如果是西经则为负
     * @param {number} n1 点1的北纬, 单位:角度, 如果是南纬则为负
     * @param {number} e2
     * @param {number} n2
     */
    getDistance = (e1, n1, e2, n2) => {
        const R = 6371;
        const { sin, cos, asin, PI, hypot } = Math;
        /** 根据经纬度获取点的坐标 */
        const getPoint = (e, n) => {
            e *= PI / 180
            n *= PI / 180
            //这里 R* 被去掉, 相当于先求单位圆上两点的距, 最后会再将这个距离放大 R 倍
            return { x: cos(n) * cos(e), y: cos(n) * sin(e), z: sin(n) }
        }

        let a = getPoint(e1, n1)
        let b = getPoint(e2, n2)
        let c = hypot(a.x - b.x, a.y - b.y, a.z - b.z)
        let r = asin(c / 2) * 2 * R
        return r
    }

    render() {
        let currentDistance = '';
        if(this.props.currentLatlng.lat && this.props.currentLatlng.lng) {
            currentDistance = this.getDistance(this.props.currentLatlng.lat , this.props.currentLatlng.lng , 30.36604845820141 , 112.08174705505373);
        }
        let selectedDistance = '';
        if(this.props.selectedLatlng.lat && this.props.selectedLatlng.lng) {
            selectedDistance = this.getDistance(this.props.selectedLatlng.lat , this.props.selectedLatlng.lng , 30.36604845820141 , 112.08174705505373);
        }
        return (
            <div id='offlineMap'>
                <Tabs defaultActiveKey='1' onChange={this.callback} size='large'>
                    <TabPane tab="&emsp;荆州&emsp;" key='1'>
                        <div className='map' style={{ margin: 'auto' }}>
                            <Map />
                            <div style={{ position: 'relative', left: '2%', display: 'inline-block' }}>当前经度:&nbsp;<Input disabled style={{ width: '350px' }} value={this.props.currentLatlng.lat} /></div>&emsp;
                            <div style={{ position: 'relative', left: '2%', display: 'inline-block' }}>当前纬度:&nbsp;<Input disabled style={{ width: '350px' }} value={this.props.currentLatlng.lng} /></div>&emsp;
                            <div style={{ position: 'relative', left: '2%', display: 'inline-block' }}>距离学校:&nbsp;<Input disabled style={{ width: '140px' }} value={currentDistance}/></div>
                            <br /><br />
                            <div style={{ position: 'relative', left: '2%', display: 'inline-block' }}>标记经度:&nbsp;<Input disabled style={{ width: '350px' }} value={this.props.selectedLatlng.lat}/></div>&emsp;
                            <div style={{ position: 'relative', left: '2%', display: 'inline-block' }}>标记纬度:&nbsp;<Input disabled style={{ width: '350px' }} value={this.props.selectedLatlng.lng}/></div>&emsp;
                            <div style={{ position: 'relative', left: '2%', display: 'inline-block' }}>距离学校:&nbsp;<Input disabled style={{ width: '140px' }} value={selectedDistance}/></div>
                        </div>
                    </TabPane>
                </Tabs>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    currentLatlng: state.mapReducer.currentLatlng,
    selectedLatlng: state.mapReducer.selectedLatlng
})

export default connect(mapStateToProps, null)(index);
