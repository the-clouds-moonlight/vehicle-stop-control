/*
 * @Author: wuxiangyi
 * @Date: 2022-03-17 10:51:01
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-03-17 20:16:37
 * @FilePath: /ReactMiddelware/src/pages/OfflineMap/Map/store/reducer.js
 * @Description: 
 * 
 */

const MapReducer = (state={}, action) => {
    let { type, data } = action;
    switch (type) {
        case 'saveCurrentLatlng':
            return {
                ...state,
                currentLatlng: data.currentLatlng
            }
        case 'saveSelectedLatlng':
            return {
                ...state,
                selectedLatlng: data.selectedLatlng
            }
        default:
            return state;
    }
}
export default MapReducer;