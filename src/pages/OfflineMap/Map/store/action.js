/*
 * @Author: wuxiangyi
 * @Date: 2022-03-17 11:09:10
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-03-17 20:15:23
 * @FilePath: /ReactMiddelware/src/pages/OfflineMap/Map/store/action.js
 * @Description: 
 * 
 */

// 保存当前经纬度
export const saveCurrentLatlng = (data) => {
    return (dispatch) => {
        dispatch({
            type: 'saveCurrentLatlng',
            data
        })
    }
}

// 保存标记经纬度
export const saveSelectedLatlng = (data) => {
    return (dispatch) => {
        dispatch({
            type: 'saveSelectedLatlng',
            data
        })
    }
}