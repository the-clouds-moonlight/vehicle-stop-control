/*
 * @Author: wuxiangyi
 * @Date: 2022-03-16 19:21:42
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-03-20 11:02:13
 * @FilePath: /ReactMiddelware/ReactMiddelware/src/pages/OfflineMap/Map/index.jsx
 * @Description: 
 * 
 */
import React, { Component } from 'react'
import $ from 'jquery';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css'
import mapIcon from '../../../assets/location-2.png'
import mapIconBlue from '../../../assets/location.png'
import { connect } from 'react-redux'
import { saveCurrentLatlng, saveSelectedLatlng } from './store/action'

class index extends Component {
    state = {
        // 默认中⼼点
        defaultCenter: {
            lag: 30.36604845820141,
            lng: 112.08174705505373
        }
    }

    componentDidMount() {
        this._map = null;
        this.glayer_normal = null;
        this.glayer_satelite = null;
        var url_normal = '/api/server/web/rest/offlineMap/788865972/{z}/{x}/{y}';
        this.glayer_normal = new L.TileLayer(url_normal, { minZoom: 10, maxZoom: 18, attribution: '普通地图' });
        // 设置地图高度
        $(window).height(function () {
            $("#mainmap").css("height", '55vh');
        });
        $("#mainmap").empty();
        this._map = new L.Map('mainmap', { layers: [this.glayer_normal] });
        this.init();
    }

    // 地图组件初始化
    init = () => {
        const { defaultCenter } = this.state;
        this._map.setView(L.latLng(defaultCenter.lag, defaultCenter.lng), 14);
        // 自定义标记
        let myIcon = L.icon({
            iconUrl: mapIcon,       //图片url
            iconSize: [35, 40],     //设置图片大小 宽度35,高度40
            iconAnchor: [20, 40],   //设置定位点的位置。默认为中心  例子中以左上角为定位参考。相当于relative
            popupAnchor: [50, 0],   //设置警示框位置 ，以iconAnchor的值进行定位。相当于absolute 例子中的警示框定位到有、右上角。
        });
        // 插标记点
        let marker = L.marker([30.3674, 112.0805], {
            title: '荆州学院太湖港校区',
            draggable: false,        // 是否可以移动
            icon: myIcon,
        });
        marker.addTo(this._map).bindPopup("<b>荆州学院</b>太湖港校区");
        // 给地图组件添加点击事件
        this._map.on('click', (event) => {
            let lat = event.latlng.lat;
            let lng = event.latlng.lng;
            // 生成标记
            let myIcon = L.icon({
                iconUrl: mapIconBlue,
                iconSize: [35, 40],
                iconAnchor: [20, 40],
                popupAnchor: [50, 0]
            })
            if (this.selectedMarker) {
                this._map.removeLayer(this.selectedMarker);
            }
            this.selectedMarker = L.marker([lat, lng], {
                title: '锁定标识',
                draggable: true,
                icon: myIcon,
            })
            this.selectedMarker.addTo(this._map).bindPopup("<b>用户标识</b>");
            // 给标识移除事件，无参代表移除所有事件
            this.selectedMarker.off();
            // 实现鼠标按下拖拽功能
            this.selectedMarker.on('drag' , (event) => {
                if(this.selectedLine) {
                    this._map.removeLayer(this.selectedLine);
                    let {lat , lng} = event.latlng;
                    this.selectedLine = L.polyline([[lat, lng], [30.36604845820141, 112.08174705505373]], {
                        color: 'red',
                        draggable: true,
                        dashArray: '15'
                    }).bindPopup();
                    this.selectedLine.addTo(this._map).bindPopup("标识线");
                    let data = {
                        selectedLatlng: {
                            lat, lng
                        }
                    }
                    this.props.saveSelectedLatlng(data);
                }
            })
            // 生成标记线
            if (this.selectedLine) {
                this._map.removeLayer(this.selectedLine);
            }
            this.selectedLine = L.polyline([[lat, lng], [30.36604845820141, 112.08174705505373]], {
                color: 'red',
                draggable: true,
                dashArray: '15'
            }).bindPopup();
            this.selectedLine.addTo(this._map).bindPopup("标识线");
            let data = {
                selectedLatlng: {
                    lat, lng
                }
            }
            this.props.saveSelectedLatlng(data);
        })
        // 给地图组件添加鼠标移动事件
        this._map.on('mousemove', (event) => {
            let lat = event.latlng.lat;
            let lng = event.latlng.lng;
            let data = {
                currentLatlng: {
                    lat, lng
                }
            }
            this.props.saveCurrentLatlng(data);
        })
    };

    render() {
        return (
            <div style={{ padding: 24, }}>
                <div id="mainmap" className="logo" onClick={this.doMark} />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    currentLatlng: state.mapReducer.currentLatlng,
    selectedLatlng: state.mapReducer.selectedLatlng
})

const mapDispatchToProps = (dispatch) => ({
    saveCurrentLatlng(data) {
        dispatch(saveCurrentLatlng(data));
    },
    saveSelectedLatlng(data) {
        dispatch(saveSelectedLatlng(data));
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(index);
