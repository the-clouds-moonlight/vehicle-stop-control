/*
 * @Author: wuxiangyi
 * @Date: 2022-03-24 21:11:33
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-03-24 21:28:38
 * @FilePath: /ReactMiddelware/src/pages/POIDemo/index.jsx
 * @Description: 
 * 
 */
import React, { Component } from 'react'
import { Card, Button } from 'antd'
import {getPDF , getWord} from './store/action'
import {connect} from 'react-redux'

class POIDemo extends Component {
    // 下载pdf
    downloadPDFFile = () => {
        this.props.getPDF();
    }

    // 下载word
    downloadWordFile = () => {
        this.props.getWord();
    }

    render() {
        return (
            <div>
                <Card title="PDF">
                    <Button onClick={this.downloadPDFFile} type='primary'>下载PDF文档</Button>
                </Card>
                <Card title="Word">
                    <Button onClick={this.downloadWordFile} type='primary'>下载Word文档</Button>
                </Card>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    pdfFilePath: state.poiReducer.pdfFilePath,
    wordFilePath: state.poiReducer.wordFilePath
})

const mapDispatchToProps = (dispatch) => ({
    getWord() {
        dispatch(getWord())
    },
    getPDF() {
        dispatch(getPDF())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(POIDemo);