/*
 * @Author: wuxiangyi
 * @Date: 2022-03-24 21:14:57
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-03-24 21:22:03
 * @FilePath: /ReactMiddelware/src/pages/POIDemo/store/action.js
 * @Description: 
 * 
 */
import * as actionType from './actionType'
import { message } from 'antd';
import request from '../../../utils/request';

/**
 * 获取PDF
 */
export const getPDF = () => {
    return (dispatch) => {
        request('POST', '/api/server/web/rest/pdfRest/getPdfFile', {
            type: 'request',
            cmd: 'getPdfFile',
            request: {}
        }).then((res) => {
            if (res.response.res) {
                dispatch({
                    type: actionType.GETPDF,
                    data: res.response.message.filePath
                });
                // 这里不直接用window.location.href 是因为浏览器会直接打开pdf而不会下载
                var link = document.createElement('a');
                link.href = '/api/server' + res.response.message.filePath
                link.download = "电子工牌.pdf";
                link.click()
                link.remove();
                // window.location.href = '/api/server' + res.response.message.filePath;
            } else {
                message.error("获取服务器资源失败！")
            }
        }).catch(err => console.log(err));
    }
}

/**
 * 获取word
 */
export const getWord = () => {
    return (dispatch) => {
        request('POST', '/api/server/web/rest/wordRest/getWordFile', {
            type: 'request',
            cmd: 'getWordFile',
            request: {}
        }).then((res) => {
            if (res.response.res) {
                dispatch({
                    type: actionType.GETWORD,
                    data: res.response.message.filePath
                });
                window.location.href = '/api/server' + res.response.message.filePath;
            } else {
                message.error("获取服务器资源失败！")
            }
        }).catch(err => console.log(err));
    }
}