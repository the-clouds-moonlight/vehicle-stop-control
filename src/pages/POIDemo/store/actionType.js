/*
 * @Author: wuxiangyi
 * @Date: 2022-03-24 21:15:02
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-03-24 21:17:12
 * @FilePath: /ReactMiddelware/src/pages/POIDemo/store/actionType.js
 * @Description: 
 * 
 */
export const GETPDF = "getPdf";
export const GETWORD = "getWord";