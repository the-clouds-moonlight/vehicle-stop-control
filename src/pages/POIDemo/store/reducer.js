/*
 * @Author: wuxiangyi
 * @Date: 2022-03-24 21:15:07
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-03-24 21:17:34
 * @FilePath: /ReactMiddelware/src/pages/POIDemo/store/reducer.js
 * @Description: 
 * 
 */
import * as actionType from './actionType'
const POIReducer = (state={} , action) => {
    let {type , data} = action;
    switch(type) {
        case actionType.GETPDF:
            return {
                ...state,
                pdfFilePath: data
            }
        case actionType.GETWORD:
            return {
                ...state,
                wordFilePath: data
            }
        default:
            return state;
    }
}
export default POIReducer;