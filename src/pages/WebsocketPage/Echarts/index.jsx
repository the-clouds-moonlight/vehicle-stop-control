// import React, { Component } from 'react';
// import Websocket from '../../../component/Websocket/websocket';
// import { Input, Button, Card, Typography } from 'antd';
// import * as echarts from 'echarts';

// var myChart;
// // const { TextArea } = Input;
// const symbolSize = 2.2;
// // 判断标识
// var judge = 0;
// const data = [];

// class Echarts extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             abscissa_ordinate: [],
//         };
//     }
//     getOption() {
//         const option = {
//             title: {
//                 text: '实时数据',
//                 left: 'center',
//             },
//             tooltip: {
//                 // triggerOn: 'click',
//                 formatter: function (params) {
//                     return (
//                         'X: ' + params.data[0].toFixed(4) + '<br>Y: ' + params.data[1].toFixed(4)
//                     );
//                 },
//                 axisPointer: {
//                     type: 'cross',
//                     link: { xAxisIndex: 'all' },
//                     label: {
//                         backgroundColor: '#6a7985',
//                     },
//                 },
//             },
//             grid: {
//                 top: '8%',
//                 bottom: '20%',
//             },
//             xAxis: {
//                 type: 'value',
//                 axisLabel: {
//                     interval: 'auto',
//                     rotate: 90,
//                 },
//             },
//             yAxis: {
//                 type: 'value',
//                 axisLabel: {
//                     interval: 'auto',
//                     // rotate: 90,
//                 },
//             },
//             dataZoom: [
//                 {
//                     type: 'slider',
//                     xAxisIndex: 0,
//                     filterMode: 'none',
//                 },
//                 {
//                     type: 'slider',
//                     yAxisIndex: 0,
//                     filterMode: 'none',
//                 },
//                 {
//                     type: 'inside',
//                     xAxisIndex: 0,
//                     filterMode: 'none',
//                 },
//                 {
//                     type: 'inside',
//                     yAxisIndex: 0,
//                     filterMode: 'none',
//                 },
//             ],
//             series: [
//                 {
//                     id: 'a',
//                     type: 'line',
//                     smooth: true,
//                     symbolSize: symbolSize,
//                     data: data,
//                 },
//             ],
//         };
//         myChart = echarts.init(document.getElementById('AdmittanceData'));
//         myChart.setOption(option);
//         window.onresize = function () {
//             myChart.resize();
//         };
//     }
//     componentDidMount() {
//         console.log('====================================');
//         console.log("websocketDidMount:",this.props.websocketData);
//         console.log('====================================');
//         this.getOption();
//     }
//     componentDidUpdate(nextPorps) {
//         const { websocketData } = this.props.websocketData;
//         console.log('====================================');
//         console.log("nextPorps:",nextPorps);
//         console.log('====================================');
//         if(nextPorps != null && nextPorps != undefined){
//         console.log('====================================');     
//         console.log("websocketData:",JSON.parse(nextPorps.websocketData));
//         console.log('====================================');
//         }
//             const { data, type } = nextPorps.websocketData;
//             console.log('====================================');
//             console.log("type",nextPorps.websocketData.type);
//             console.log("data",nextPorps.websocketData.data);
//             console.log('====================================');
//             if (type === 'echartsData' && data != null && data != undefined) {
//                 var echartsData = JSON.parse(data);
//                 // if(data.number == 0){
//                 //     setAbscissa_ordinate([]);
//                 // }
//                 // 获取X轴数据
//                 const [...abscissaList] = echartsData.abscissaList;
//                 // 获取Y轴数据
//                 const [...ordinateList] = echartsData.ordinateList;
//                 var newData = [];
//                 for (let index = 0; index < abscissaList.length; index++) {
//                     let temporaryData = [];
//                     temporaryData.push(abscissaList[index]);
//                     temporaryData.push(ordinateList[index]);
//                     // setAbscissa_ordinate(abscissa_ordinate.concat(temporaryData));
//                     newData.push(temporaryData);
//                 }
//                 // 将数据拼接到state中
//                 var newabscissa_ordinate = this.state.abscissa_ordinate.concat(newData);
//                 console.log('====================================');
//                 console.log("newData:",newData);
//                 console.log('====================================');
//                 this.setState({
//                     abscissa_ordinate: newabscissa_ordinate,
//                 });
//                 // 判断标识+1
//                 judge = judge + 1;
//                 setTimeout(function () {
//                     // Add shadow circles (which is not visible) to enable drag.
//                     myChart.setOption({
//                         graphic: newabscissa_ordinate.map(function (item, dataIndex) {
//                             return {
//                                 type: 'circle',
//                                 position: myChart.convertToPixel('grid', item),
//                                 shape: {
//                                     cx: 0,
//                                     cy: 0,
//                                     r: symbolSize / 2,
//                                 },
//                                 invisible: true,
//                                 draggable: true,
//                                 ondrag: function (dx, dy) {
//                                     onPointDragging(dataIndex, [this.x, this.y]);
//                                 },
//                                 onmousemove: function () {
//                                     showTooltip(dataIndex);
//                                 },
//                                 onmouseout: function () {
//                                     hideTooltip(dataIndex);
//                                 },
//                                 z: 100,
//                             };
//                         }),
//                     });
//                 }, 0);
//                 window.addEventListener('resize', updatePosition);
//                 myChart.on('dataZoom', updatePosition);
//                 function updatePosition() {
//                     myChart.setOption({
//                         graphic: newabscissa_ordinate.map(function (item, dataIndex) {
//                             return {
//                                 position: myChart.convertToPixel('grid', item),
//                             };
//                         }),
//                     });
//                 }
//                 function showTooltip(dataIndex) {
//                     myChart.dispatchAction({
//                         type: 'showTip',
//                         seriesIndex: 0,
//                         dataIndex: dataIndex,
//                     });
//                 }
//                 function hideTooltip(dataIndex) {
//                     myChart.dispatchAction({
//                         type: 'hideTip',
//                     });
//                 }
//                 function onPointDragging(dataIndex, pos) {
//                     newabscissa_ordinate[dataIndex] = myChart.convertFromPixel('grid', pos);
//                     // Update data
//                     myChart.setOption({
//                         series: [
//                             {
//                                 id: 'a',
//                                 data: newabscissa_ordinate,
//                             },
//                         ],
//                     });
//                 }
//             }
//             var newOption = {
//                 series: [
//                     {
//                         id: 'a',
//                         type: 'line',
//                         smooth: true,
//                         symbolSize: symbolSize,
//                         data: newabscissa_ordinate,
//                     },
//                 ],
//             };
//             return myChart.setOption(newOption);
        
//     }

//     render() {
//         return (
//             <>
//                 <Websocket />
//                 <div id="AdmittanceData" style={{ width: '80vw', height: '70vh' }}></div>
//             </>
//         );
//     }
// }

// export default Echarts;
