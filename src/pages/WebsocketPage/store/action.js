import request from '@utils/request';

//  获取本地token 默认为undefined
const token = localStorage.getItem("x-auth-token") || "undefined";

export const testWebsocket = () => {
  return () => {
    request('GET', `/api/server/web/rest/testWebsocket/test?token=${token}`).catch(err=>console.log(err))
  }
}