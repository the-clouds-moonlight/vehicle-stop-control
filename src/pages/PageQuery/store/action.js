import * as ActionTypes from './actionType';
import request from '@utils/request';
import { message } from 'antd';

/**
 * 分页查询
 * @param {JSON} data 
 * @returns 
 */
export const pageQuery = (data) => {
  return (dispatch) => {
    request('POST', '/api/server/web/rest/pageRest/pageQuery', {
      cmd: 'pageQuery',
      type: 'request',
      request: {
        ...data
      }
    }).then(obj => {
      let res = obj.response;
      if (res.res) {
        dispatch(pageQueryReducer(res.message))
      } else {
        message.error(res.exception, 1);
      }
    }).catch(err => console.log(err));
  }
}

const pageQueryReducer = (data) => ({
  type: ActionTypes.PAGEQUERY,
  data
})