import * as ActionType from './actionType';

const initialState = {}

export default (state = initialState, { type, data }) => {
  switch (type) {

  case ActionType.PAGEQUERY:
    return { ...state, data: data }
  default:
    return state
  }
}