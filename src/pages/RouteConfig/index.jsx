import React, { Component } from 'react'
import { Card } from 'antd'
import RouteTable from './RouteTable'
import { connect } from 'react-redux'
import * as actions from './store/action'

class RouteConfig extends Component {

    componentDidMount() {
        this.props.getRoutes();
    }

    render() {

        const { data } = this.props;

        // const data = [
        //     {
        //         key: '1',
        //         path: '/layout',
        //         name: '边栏1',
        //         icon: 'PieChartOutlined',
        //         strict: true,
        //         children: [
        //             {
        //                 key: '1-1',
        //                 path: '/layout/one',
        //                 name: '边栏1-1',
        //                 icon: 'UserOutlined',
        //                 strict: true,
        //                 children: [
        //                     {
        //                         key: '1-1-1',
        //                         path: '/layout/one/one',
        //                         name: '边栏1-1',
        //                         icon: 'UserOutlined',
        //                         strict: true,
        //                         component: 'Basic',
        //                     },
        //                     {
        //                         key: '1-1-2',
        //                         path: '/layout/one',
        //                         exact: true,
        //                         redirect: '/layout/one/one'
        //                     },
        //                     { key: '1-1-3', path: '*', exact: true, redirect: '/exception' }
        //                 ]
        //             },
        //             {
        //                 key: '1-2',
        //                 path: '/layout/two',
        //                 name: '边栏1-2',
        //                 strict: true,
        //                 icon: 'UserOutlined',
        //                 component: 'BasicTwo',
        //             },
        //             { key: '1-3', path: '*', exact: true, redirect: '/exception' }
        //         ]
        //     },
        //     {
        //         key: '2',
        //         path: '/layout2',
        //         name: '边栏2',
        //         icon: 'UserOutlined',
        //         component: 'BasicTwo'
        //     },
        //     { path: '/', exact: true, redirect: '/layout/one/one', key: '3', },
        //     { path: '*', exact: true, redirect: '/exception', key: '4', }
        // ]

        return (
            <Card>
                <RouteTable
                    data={data}
                    createRoute={this.createRoute}
                    changeRoute={this.changeRoute}
                    deleteRoute={this.deleteRoute}
                />
            </Card>
        )
    }

    deleteRoute = (key) => {
        console.log('删除路由', key)
        this.props.deleteRoutes(key)
    }

    createRoute = (value) => {
        console.log('创建路由', value)
        console.log(JSON.stringify(value))
        this.props.createRoutes(value)
    }

    changeRoute = (value) => {
        console.log('修改路由', value)
        this.props.changeRoutes(value)
    }

}

const mapStateToProps = (state) => ({
    data: state.routerConfig.data
})

const mapDispatchToProps = (dispatch) => ({
    getRoutes() {
        dispatch(actions.getRouterRequest(false))
    },
    createRoutes(value) {
        dispatch(actions.createRouterRequest(value))
    },
    changeRoutes(value) {
        dispatch(actions.changeRouterRequest(value))
    },
    deleteRoutes(id) {
        dispatch(actions.deleteRouterRequest(id))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(RouteConfig);
