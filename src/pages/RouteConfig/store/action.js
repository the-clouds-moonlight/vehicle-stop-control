import request from '@utils/request';
import * as actionTypes from './actionType'
import { message } from 'antd';

//获取动态路由
export const getRouterRequest = () => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/routerrest/findRouter', {
            cmd: 'findRouter',
            type: 'request',
            request: {
                isLogin: false
            }
        }).then(obj => {
            console.log(obj, 'routes')
            let res = obj.response;
            if (res.res) {
                // message.success('获取路由成功', 1);
                dispatch(getRouterReducer(res.message))
            } else {
                message.error('获取路由失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

const getRouterReducer = (data) => ({
    type: actionTypes.ROUTECONFIG_GET_ROUTE,
    data
})

export const createRouterRequest = (value) => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/routerrest/addRouter', {
            cmd: 'addRouter',
            type: 'request',
            request: {
                ...value
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('创建路由成功', 1);
                dispatch(getRouterRequest())
            } else {
                message.error('创建路由失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

export const changeRouterRequest = (value) => {
    return (dispatch) => {
        console.log(value)
        request('POST', '/api/server/web/rest/routerrest/editRouter', {
            cmd: 'editRouter',
            type: 'request',
            request: {
                ...value
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('修改路由成功', 1);
                dispatch(getRouterRequest())
            } else {
                message.error('修改路由失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

export const deleteRouterRequest = (id) => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/routerrest/deleteRouter', {
            cmd: 'deleteRouter',
            type: 'request',
            request: {
                id
            }
        }).then(obj => {
            let res = obj.response;
            if (res.res) {
                message.success('删除路由成功', 1);
                dispatch(getRouterRequest())
            } else {
                message.error('删除路由失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

