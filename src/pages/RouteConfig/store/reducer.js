import * as actionType from './actionType';

export default (state = {}, action) => {
    switch (action.type) {
        case actionType.ROUTECONFIG_GET_ROUTE: {
            return { ...state, data: action.data }
        }
        default:
            return state;
    }
}