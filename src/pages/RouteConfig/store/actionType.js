export const ROUTECONFIG_GET_ROUTE = 'routerConfig/getRoute';
export const ROUTECONFIG_CREATE_ROUTE = 'routerConfig/createRoute';
export const ROUTECONFIG_CHANGE_ROUTE = 'routerConfig/changeRoute';
export const ROUTECONFIG_DELETE_ROUTE = 'routerConfig/deleteRoute';