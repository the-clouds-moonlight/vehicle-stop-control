import React, { Component, Fragment } from 'react'
import { Space, Button, Table, Drawer, Modal, Typography, Form, TreeSelect, Input, Select } from 'antd'
import {
    EditOutlined,
    DeleteOutlined
} from '@ant-design/icons';

export default class RouteTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            codeVisible: false,
            modalVisible: false,

            actionType: 1, //1‘新建 2’修改
            renderData: {},
            actionKey: undefined,
        }
    }

    render() {

        const { codeVisible, modalVisible, actionType, renderData } = this.state;
        const { data } = this.props;

        const columns = [
            {
                title: '序号',
                dataIndex: 'id',
                key: 'id',
            },
            {
                title: '排序权重',
                dataIndex: 'sort',
                key: 'sort'
            },
            {
                title: 'path',
                dataIndex: 'path',
                key: 'path',
            },
            {
                title: 'component',
                dataIndex: 'component',
                key: 'component',
            },
            {
                title: 'name',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: 'icon',
                dataIndex: 'icon',
                key: 'icon',
            },
            {
                title: 'strict',
                dataIndex: 'strict',
                key: 'strict',
                render: (text) => {
                    if (text) {
                        return (
                            <span>true</span>
                        )
                    } else {
                        return (
                            <span>false</span>
                        )
                    }
                }
            },
            {
                title: 'exact',
                dataIndex: 'exact',
                key: 'exact',
                render: (text) => {
                    if (text) {
                        return (
                            <span>true</span>
                        )
                    } else {
                        return (
                            <span>false</span>
                        )
                    }
                }
            },
            {
                title: 'redirect',
                dataIndex: 'redirect',
                key: 'redirect',
            },
            {
                title: '操作',
                key: 'action',
                dataIndex: 'action',
                render: (text, record) => {
                    return (
                        <Space>
                            <Button type='primary' icon={<EditOutlined />} ghost style={{ borderColor: 'green', color: 'green' }} onClick={() => this.setState({ actionType: 2, modalVisible: true, renderData: record, actionKey: record.key })} ></Button>
                            <Button type='primary' icon={<DeleteOutlined />} ghost danger onClick={() => this.deleteConfirm(record.key, record.path)} ></Button>
                        </Space>
                    )
                }
            }
        ];



        return (
            <Fragment>
                <Space>
                    <Button type='primary' ghost onClick={() => this.setState({ actionType: 1, modalVisible: true })}>
                        添加路由
                </Button>
                    <Button type='primary' onClick={() => this.setState({ codeVisible: true })} ghost>
                        查看路由结构
                </Button>
                </Space>
                <Table columns={columns} dataSource={data} style={{ marginTop: 10 }} />
                <Drawer
                    visible={codeVisible}
                    title='路由树形结构'
                    onClose={() => this.setState({ codeVisible: false })}
                    placement='right'
                    width={'30%'}
                    destroyOnClose
                >
                    <pre>
                        {JSON.stringify(data, null, 2)}
                    </pre>
                </Drawer>
                <Modal
                    title={actionType === 1 ? '新建路由' : '修改路由'}
                    visible={modalVisible}
                    destroyOnClose
                    centered
                    onCancel={() => this.setState({ modalVisible: false })}
                    footer={null}
                >
                    {
                        actionType === 1 ? (
                            <Form
                                onFinish={this.onFormCreateOk}
                                fields={
                                    [
                                        { name: ['strict'], value: false },
                                        { name: ['exact'], value: false }
                                    ]
                                }
                            >
                                <Form.Item
                                    label='父级路由'
                                    name={['id']}
                                    rules={[{ required: true, message: '请选择父级路由' }]}
                                >
                                    <TreeSelect
                                        style={{ width: 170 }}
                                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                        placeholder="请选择父级路由"
                                        treeDefaultExpandAll
                                    >
                                        <TreeSelect.TreeNode value={-1} title='根路由' />
                                        {this.renderTreeDataNode(data)}
                                    </TreeSelect>
                                </Form.Item>
                                <Form.Item
                                    label='排序权重'
                                    name={['sort']}
                                    rules={[{ required: true, message: '请填写排序权重(权重越大路由越靠后)' }]}
                                >
                                    <Input placeholder='请填写排序权重(权重越大路由越靠后)' />
                                </Form.Item>
                                <Form.Item
                                    label='path'
                                    name={['path']}
                                    rules={[{ required: true, message: '请填写路由path' }]}
                                >
                                    <Input placeholder='请填写路由path' />
                                </Form.Item>
                                <Form.Item
                                    label='component'
                                    name={['component']}
                                >
                                    <Input placeholder='请填写component(位于pages文件夹下的文件名称)' />
                                </Form.Item>
                                <Form.Item
                                    label='name'
                                    name={['name']}
                                >
                                    <Input placeholder='请填写name' />
                                </Form.Item>
                                <Form.Item
                                    label='icon'
                                    name={['icon']}
                                >
                                    <Input placeholder='请填写icon' />
                                </Form.Item>
                                <Form.Item
                                    label='strict'
                                    name={['strict']}
                                >
                                    <Select
                                        placeholder='请选择是否严格匹配/(如不需要请选择false)'
                                    >
                                        <Select.Option value={false}>false</Select.Option>
                                        <Select.Option value={true}>true</Select.Option>
                                    </Select>
                                </Form.Item>
                                <Form.Item
                                    label='exact'
                                    name={['exact']}
                                >
                                    <Select
                                        placeholder='请选择是否精确匹配路由(如不需要请选择false)'
                                    >
                                        <Select.Option value={false}>false</Select.Option>
                                        <Select.Option value={true}>true</Select.Option>
                                    </Select>
                                </Form.Item>
                                <Form.Item
                                    label='redirect'
                                    name={['redirect']}
                                >
                                    <Input placeholder='请填写路由重定向的路径' />
                                </Form.Item>
                                <Form.Item>
                                    <Space style={{ float: 'right' }}>
                                        <Button type='primary' ghost htmlType='submit'>提交</Button>
                                        <Button type='primary' ghost htmlType='button' onClick={() => this.setState({ modalVisible: false })} danger>取消</Button>
                                    </Space>
                                </Form.Item>

                            </Form>
                        ) : (
                                <Form
                                    onFinish={this.onFormChangeOk}
                                    fields={
                                        [
                                            { name: ['parentId'], value: renderData.parentId },
                                            { name: ['path'], value: renderData.path },
                                            { name: ['sort'], value: renderData.sort },
                                            { name: ['component'], value: renderData.component },
                                            { name: ['name'], value: renderData.name },
                                            { name: ['icon'], value: renderData.icon },
                                            { name: ['strict'], value: renderData.strict },
                                            { name: ['exact'], value: renderData.exact },
                                            { name: ['redirect'], value: renderData.redirect },
                                        ]
                                    }
                                >
                                    <Form.Item
                                        label='父级路由'
                                        name={['parentId']}
                                        rules={[{ required: true, message: '请选择父级路由' },
                                        {
                                            validator: (rule, value, callback) => {
                                                // console.log(value, getFieldValue('newpassword'))
                                                if (value === this.state.actionKey) {
                                                    callback('不能选择自己作为父节点')
                                                }
                                                callback()
                                            }
                                        }
                                        ]}
                                    >
                                        <TreeSelect
                                            style={{ width: 170 }}
                                            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                            placeholder="请选择父级路由"
                                            treeDefaultExpandAll
                                        >
                                            <TreeSelect.TreeNode value={-1} title='根路由' />
                                            {this.renderTreeDataNode(data)}
                                        </TreeSelect>
                                    </Form.Item>
                                    <Form.Item
                                        label='path'
                                        name={['path']}
                                        rules={[{ required: true, message: '请填写路由path' }]}
                                    >
                                        <Input placeholder='请填写路由path' />
                                    </Form.Item>
                                    <Form.Item
                                        label='排序权重'
                                        name={['sort']}
                                        rules={[{ required: true, message: '请填写排序权重(权重越大路由越靠后)' }]}
                                    >
                                        <Input placeholder='请填写排序权重(权重越大路由越靠后)' />
                                    </Form.Item>
                                    <Form.Item
                                        label='component'
                                        name={['component']}
                                    >
                                        <Input placeholder='请填写component(位于pages文件夹下的文件名称)' />
                                    </Form.Item>
                                    <Form.Item
                                        label='name'
                                        name={['name']}
                                    >
                                        <Input placeholder='请填写name' />
                                    </Form.Item>
                                    <Form.Item
                                        label='icon'
                                        name={['icon']}
                                    >
                                        <Input placeholder='请填写icon' />
                                    </Form.Item>
                                    <Form.Item
                                        label='strict'
                                        name={['strict']}
                                    >
                                        <Select
                                            placeholder='请选择是否严格匹配/(如不需要请选择false)'
                                        >
                                            <Select.Option value={false}>false</Select.Option>
                                            <Select.Option value={true}>true</Select.Option>
                                        </Select>
                                    </Form.Item>
                                    <Form.Item
                                        label='exact'
                                        name={['exact']}
                                    >
                                        <Select
                                            placeholder='请选择是否精确匹配路由(如不需要请选择false)'
                                        >
                                            <Select.Option value={false}>false</Select.Option>
                                            <Select.Option value={true}>true</Select.Option>
                                        </Select>
                                    </Form.Item>
                                    <Form.Item
                                        label='redirect'
                                        name={['redirect']}
                                    >
                                        <Input placeholder='请填写路由重定向的路径' />
                                    </Form.Item>
                                    <Form.Item>
                                        <Space style={{ float: 'right' }}>
                                            <Button type='primary' ghost htmlType='submit'>提交</Button>
                                            <Button type='primary' ghost htmlType='button' onClick={() => this.setState({ modalVisible: false })} danger>取消</Button>
                                        </Space>
                                    </Form.Item>
                                </Form>
                            )
                    }
                </Modal>
            </Fragment>
        )
    }

    renderTreeDataNode = (data = []) => {
        const { TreeNode } = TreeSelect;
        return data.filter(item => item.path && item.name && item.icon).map(item => {
            if (item.children) {
                return (
                    <TreeNode title={<span>{item.name}</span>} key={item.key} value={item.key}>
                        {this.renderTreeDataNode(item.children)}
                    </TreeNode>

                );
            }
            return <TreeNode key={item.key} title={<span>{item.name}</span>} value={item.key} />;
        })
    }

    onFormCreateOk = (value) => {
        this.setState({
            modalVisible: false
        })
        this.props.createRoute(value)
    }

    onFormChangeOk = (value) => {
        const { renderData } = this.state;
        this.setState({
            modalVisible: false
        })
        const value2 = {
            ...value,
            id: renderData.id
        }
        this.props.changeRoute(value2)
    }

    //删除警告弹窗
    deleteConfirm(key, text) {
        Modal.confirm({
            title: '确认',
            content: (
                <div>
                    是否删除该路由
                    <Typography.Text strong type="danger">
                        {text}
                    </Typography.Text>
                            的信息
                </div>
            ),
            okText: '确认',
            okType: 'danger',
            cancelText: '取消',
            onOk: () => {
                this.props.deleteRoute(key)
            },
            onCancel: () => {

            }
        })
    }

}
