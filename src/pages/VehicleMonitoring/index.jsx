/*
 * @Author: wuxiangyi
 * @Date: 2022-05-16 14:19:27
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-05-16 14:19:53
 * @FilePath: /VehicleStopControl/src/pages/VehicleMonitoring/index.jsx
 * @Description: 
 * 
 */
import React, { Component } from 'react'
import { Card, Divider, Input, Button, Space, Table, Tag } from 'antd';
import './css/index.css'
import img1 from './imgs/iShot_2022-05-16_14.48.58.png'

const { Meta } = Card;

export default class index extends Component {
    render() {

        const columns = [
            {
                title: '车牌号',
                dataIndex: 'carID',
                key: 'carID',
            },
            {
                title: '进场时间',
                dataIndex: 'inTime',
                key: 'inTime',
            },
            {
                title: '出场时间',
                dataIndex: 'outTime',
                key: 'outTime',
            },
            {
                title: '停车时长',
                dataIndex: 'spaceTime',
                key: 'spaceTime',
            },
            {
                title: '车辆所属人',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: '手机号码',
                dataIndex: 'phone',
                key: 'phone',
            },
            {
                title: '操作',
                dataIndex: 'address',
                key: 'address',
                render: tags => (
                    <>
                        <Tag color="green" key={tags}>入场照片</Tag>
                        <Tag color="red" key={tags}>出场照片</Tag>
                    </>
                ),
            },
        ];

        const data = [
            {
                carID: '玛莎拉蒂',
                inTime: '2022-05-16 14:19:27',
                outTime: '2022-05-16 14:19:53',
                spaceTime: '17秒',
                name: 'wuxiangyi',
                phone: '13509876789',
            },
        ];

        return (
            <>
                <div className="CardWarpper" >
                    <Card
                        style={{ width: 300 }} cover={<img alt="example" src={img1} />}>
                        <Meta title="车牌号：玛莎拉蒂" description="2022-05-16 14:19:53 西大门" />
                    </Card>
                    <Card
                        style={{ width: 300 }} cover={<img alt="example" src={img1} />}>
                        <Meta title="车牌号：玛莎拉蒂" description="2022-05-16 14:19:53 西大门" />
                    </Card>
                    <Card
                        style={{ width: 300 }} cover={<img alt="example" src={img1} />}>
                        <Meta title="车牌号：玛莎拉蒂" description="2022-05-16 14:19:53 西大门" />
                    </Card>
                    <Card
                        style={{ width: 300 }} cover={<img alt="example" src={img1} />}>
                        <Meta title="车牌号：玛莎拉蒂" description="2022-05-16 14:19:53 西大门" />
                    </Card>
                    <Card
                        style={{ width: 300 }} cover={<img alt="example" src={img1} />}>
                        <Meta title="车牌号：玛莎拉蒂" description="2022-05-16 14:19:53 西大门" />
                    </Card>
                    <Card
                        style={{ width: 300 }} cover={<img alt="example" src={img1} />}>
                        <Meta title="车牌号：玛莎拉蒂" description="2022-05-16 14:19:53 西大门" />
                    </Card>
                </div>

                <Divider />

                <div className="InputWarpper">
                    <Space>
                        <Input addonBefore="车牌号" />
                        <Input addonBefore="车主姓名" />
                        <Input addonBefore="手机号" />
                        <Button block>
                            🔍 搜索
                        </Button>
                    </Space>
                </div>

                <Divider />

                <div className="tableWarpper">
                    <Table columns={columns} dataSource={data} />
                </div>
            </>
        )
    }
}
