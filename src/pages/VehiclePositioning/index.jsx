/*
 * @Author: wuxiangyi
 * @Date: 2022-05-16 14:24:26
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-05-16 14:24:43
 * @FilePath: /VehicleStopControl/src/pages/VehiclePositioning/index.jsx
 * @Description: 
 * 
 */
import React, { Component } from 'react'
import AMapLoader from '@amap/amap-jsapi-loader';
import { Form, Input, Button, Descriptions, Modal, Col, Row, Table } from 'antd';
import './MapContainer.css';
import map from '../../assets/map/map.png'

class MapComponent extends Component {
    constructor() {
        super();
        // this.state = {
        //     map:{}
        // };       
        this.map = {};

    }
    // 2.dom渲染成功后进行map对象的创建
    componentDidMount() {
        AMapLoader.load({
            key: 'b8fd7da98786129ccb1193434887b893', //需要设置您申请的key
            version: "2.0",
            plugins: ['AMap.ToolBar', 'AMap.Driving'],
            AMapUI: {
                version: "1.1",
                plugins: [],

            },
            Loca: {
                version: "2.0.0"
            },
        }).then((AMap) => {
            this.map = new AMap.Map("mapcontainer", {
                viewMode: "3D",
                zoom: 5,
                zooms: [2, 22],
                center: [105.602725, 37.076636],
            });
            let positionArr = [
                [113.357224, 34.977186],
                [114.555528, 37.727903],
                [112.106257, 36.962733],
                [109.830097, 31.859027],
                [116.449181, 39.98614],
            ];
            for (let item of positionArr) {
                let marker = new AMap.Marker({
                    position: [item[0], item[1]],
                });
                this.map.add(marker);
            }
        }).catch(e => {
            console.log(e);
        })
    }
    render() {
        // 1.创建地图容器
        return (
            <div className='mona'>
                <img src={map} width='80%' />

                <div className='na'>
                    <Descriptions title="车辆列表" layout="vertical" bordered>
                        <Descriptions.Item span={3} label=""> <Button style={{ marginBottom: '2vh' }}>鄂A67Q76</Button></Descriptions.Item>
                        
                        <Descriptions.Item span={3} label=""> <Button style={{ marginBottom: '2vh' }}>鄂AB7QN6</Button></Descriptions.Item>
                        <Descriptions.Item span={3} label=""> <Button style={{ marginBottom: '2vh' }}>鄂A6JQ7L</Button></Descriptions.Item>
                
                     
                    </Descriptions>
                </div>
            </div>
        );
    }

}
export default MapComponent;
