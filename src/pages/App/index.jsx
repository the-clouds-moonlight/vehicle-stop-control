import React from 'react';
import { Card } from 'antd';
import { connect } from 'react-redux';
import * as METHOD from '../../component/smartlab-login-component/index.jsx';
import { withRouter } from 'react-router-dom';
// import ImgAvatar from '../../assets/login/hander.png';
import Particles from 'react-tsparticles';
import styles from './App.less';

const tabListNoTitle = [
    {
        key: 'LoginOther',
        tab: '登录',
    },
    {
        key: 'RegisterEmail',
        tab: '注册',
    },
];

const contentListNoTitle = {
    LoginOther: (
        <div className={styles.page_body_card}>
            {/* <div className="body_card_hander">
          </div> */}
            <div className={styles.body_card_body}>
                <METHOD.LoginOther />
            </div>
        </div>
    ),
    RegisterEmail: (
        <div className={styles.page_body_card_register}>
            <div className={styles.body_card_body}>
                <METHOD.RegisterOther />
            </div>
        </div>
    ),
};

// const particlesConfig = {
//     "fps_limit": 60,
//     "interactivity": {
//         "detectsOn": "canvas",
//         "events": {
//             // "onClick": {
//             //     "enable": true,
//             //     "mode": "push"
//             // },
//             "onHover": {
//                 "enable": true,
//                 "mode": "repulse"
//             },
//             "resize": true
//         },
//         "modes": {
//             // "push": {
//             //     "particles_nb": 4
//             // },
//             "repulse": {
//                 "distance": 200,
//                 "duration": 0.4
//             }
//         }
//     },
//     "particles": {
//         "color": {
//             "value": "#ffffff"
//         },
//         "links": {
//             "color": "#ffffff",
//             "distance": 150,
//             "enable": true,
//             "opacity": 0.4,
//             "width": 1
//         },
//         "move": {
//             "bounce": false,
//             "direction": "none",
//             "enable": true,
//             "outMode": "out",
//             "random": false,
//             "speed": 2,
//             "straight": false
//         },
//         "number": {
//             "density": {
//                 "enable": true,
//                 "area": 800
//             },
//             "value": 80
//         },
//         "opacity": {
//             "value": 0.5
//         }, "shape": {
//             "type": "circle"
//         },
//         "size": {
//             "random": true,
//             "value": 5
//         }
//     },
//     "detectRetina": true
// }

class App extends React.Component {
    state = {
        noTitleKey: 'LoginOther',
    };

    onTabChange = (key, type) => {
        console.log(key, type);
        this.setState({ [type]: key });
    };

    render() {
        // console.log(process.env.REMOTE_SERVER_IP, 'remoteServerIP')
        // console.log(process.env.CLIENT_IP, 'clientIP')
        return (
            <>
                {/* <Particles width='100vw' height='100vh' params={particlesConfig} /> */}
                <div className={styles.login_main}>
                    <div className={styles.login_page}>
                        <div className={styles.login_page_header}>
                            <div className={styles.header_logo}></div>
                            <div className={styles.header_title}>Smartlab411</div>
                        </div>
                        <div className={styles.login_page_body}>
                            <Card
                                // style={{ backgroundColor: 'transparent'}}
                                tabList={tabListNoTitle}
                                activeTabKey={this.state.noTitleKey}
                                // tabBarExtraContent={<a href="#">More</a>}
                                bordered={false}
                                onTabChange={key => {
                                    this.onTabChange(key, 'noTitleKey');
                                }}
                            >
                                {contentListNoTitle[this.state.noTitleKey]}
                            </Card>
                        </div>
                        <div className={styles.login_page_tail}></div>
                    </div>
                </div>
            </>

        );
    }
}
// const App = ({ getMenu }) => {
//   return (
//     <Card>
//       <p className={styles.red}>
//         Hello
//     </p>
//       <Button onClick={() => { window.location.href = '/layout2'; getMenu() }}>跳转</Button>
//     </Card>
//   );
// }

const mapStateToProps = state => {
    return {
        isLogin: state.login.isLogin,
    };
};
export default connect(mapStateToProps, null)(withRouter(App));
