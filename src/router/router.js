import React, { lazy } from 'react'

import NormalLayout from '@layout/NormalLayout';
import BasicLayout from '@layout/BasicLayout';

import {
    UserOutlined,
    RadarChartOutlined,
    // PieChartOutlined,
} from '@ant-design/icons';

/**
 * 最外层路由必须是 path:'/'和component, 自己配置第一层路由需要在children里写
 * 如有二级路由，需要在当前object结构里写入children里添加子路由
 * 在每层路由里加入 path:'*'和redirect:'/exception'，当路由不匹配时会自动跳转到这个异常页面，防止用户自己乱输路由进行匹配
 * @author swh
 */

export const store_routes = [
    {
        path: '/BasicLayout/vehicleMonitoring',
        name: '车辆监控',
        icon: 'PieChartOutlined',
        // strict: true,
        component: lazy(() => import("@pages/VehicleMonitoring"))
    },
    { 
        path: '/BasicLayout/vehiclePositioning',
        name: '车辆定位',
        icon: 'PieChartOutlined',
        component: lazy(() => import("@pages/VehiclePositioning"))
    },
    { 
        path: '/BasicLayout/trafficManagement',
        name: '车务管理',
        icon: 'PieChartOutlined',
        component: lazy(() => import("@pages/TrafficManagement"))
    },
    // {
    //     path: '/BasicLayout/POI',
    //     name: 'POIDemo',
    //     icon: 'CopyOutlined',
    //     component: lazy(() => import('@pages/POIDemo'))
    // },
    // {
    //     path: '/BasicLayout/WebSocketPage',
    //     name: 'websocket&Echarts',
    //     icon: 'SoundOutlined',
    //     component: lazy(() => import("@pages/WebsocketPage"))
    // },
    // {
    //     path: '/BasicLayout/OfflineMap',
    //     name: '离线地图',
    //     icon: 'RadarChartOutlined',
    //     component: lazy(() => import("@pages/OfflineMap"))
    // },
    // {
    //     path: '/BasicLayout/SMSsend',
    //     name: '短信发送',
    //     icon: 'MessageOutlined',
    //     children: [
    //         {
    //             path: '/BasicLayout/SMSsend/DistanceNote',
    //             name: '远程发送短信',
    //             icon: 'MailOutlined',
    //             component: lazy(() => import("@pages/SMSsend/DistanceNote"))
    //         },
    //         {
    //             path: '/BasicLayout/SMSsend/LocalNote',
    //             name: '本地发送短信',
    //             icon: 'MobileOutlined',
    //             component: lazy(() => import("@pages/SMSsend/LocalNote"))
    //         },
    //         { path: '/BasicLayout/SMSsend', exact: true, redirect: '/BasicLayout/SMSsend/DistanceNote' }
    //     ]
    // },
    // {
    //     path: '/BasicLayout/Documents/PersonalFile',
    //     name: '文档管理',
    //     icon: 'FileOutlined',
    //     component: lazy(() => import("@pages/Documents/PersonalFile"))
    // },
    // {
    //     path: '/BasicLayout/PageQuery',
    //     name: '分页查询',
    //     icon: 'FileOutlined',
    //     component: lazy(() => import("@pages/PageQuery"))
    // },
    // {
    //     path: '/BasicLayout/Documents',
    //     name: '文档管理',
    //     icon: 'FolderOutlined',
    //     children: [
    //         {
    //             path: '/BasicLayout/Documents/PersonalFile',
    //             name: '个人文档',
    //             icon: 'FileOutlined',
    //             component: lazy(() => import("@pages/Documents/PersonalFile"))
    //         },
    //         {
    //             path: '/BasicLayout/Documents/SharedFile',
    //             name: '共享文档',
    //             icon: 'TeamOutlined',
    //             component: lazy(() => import("@pages/Documents/SharedFile"))
    //         },
    //         { path: '/BasicLayout/Documents', exact: true, redirect: '/BasicLayout/Documents/PersonalFile' }
    //     ]
    // },
    {
        path: '/BasicLayout/RouteConfig',
        name: '路由配置',
        icon: 'UserOutlined',
        component: 'RouteConfig'
    },
    {
        path: '/BasicLayout/RoleConfig',
        name: '角色配置',
        icon: 'PieChartOutlined',
        component: 'RoleConfig'
    },
    {
        path: '/BasicLayout/AccountConfig',
        name: '用户配置',
        icon: 'UserOutlined',
        component: 'AccountConfig'
    },
    { path: '/BasicLayout', exact: true, redirect: '/BasicLayout/ExcelDemo' },
    // { path: '*', redirect: '/exception' }
]

const getRouter = (routes) => {
    const router = [
        {
            path: '/',
            component: NormalLayout,
            children: [
                {
                    path: '/login',
                    name: 'app',
                    icon: <UserOutlined />,
                    component: lazy(() => import('@pages/App'))
                },
                // {
                //     path: '/2',
                //     component: BasicLayout,
                //     children: [
                //         {
                //             path: '/2/layout',
                //             name: '边栏',
                //             icon: 'PieChartOutlined ',
                //             strict: true,
                //             children: [
                //                 {
                //                     path: '/2/layout/one',
                //                     name: '边栏',
                //                     icon: 'UserOutlined',
                //                     strict: true,
                //                     component: lazy(() => import('@pages/Basic')),
                //                 },
                //                 {
                //                     path: '/2/layout/two',
                //                     name: '边栏',
                //                     strict: true,
                //                     icon: 'UserOutlined',
                //                     component: lazy(() => import('@pages/BasicTwo')),
                //                 },
                //                 { path: '*', exact: true, redirect: '/exception' }
                //             ]
                //         },
                //         {
                //             path: '/2/layout2',
                //             name: '边栏',
                //             icon: 'UserOutlined',
                //             component: lazy(() => import('@pages/BasicTwo'))
                //         },

                //         // { path: '/', exact: true, redirect: '/layout/one' },
                //         { path: '*', exact: true, redirect: '/exception' }
                //     ]
                // },
                {
                    // name: '异常页',
                    path: '/exception',
                    component: lazy(() => import('@component/NotFount'))
                },

                {
                    path: '/BasicLayout',
                    component: BasicLayout,
                    children: [
                        // ...routes,
                        ...store_routes
                    ]
                },
                { path: "/", exact: true, redirect: "/login"},
                { path: '*', redirect: '/exception' }
            ]
        }
    ]
    return router;
}


export default getRouter; 
