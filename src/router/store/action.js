import request from '@utils/request';
import { message } from 'antd';
import * as actionType from './actionType';

//获取动态路由
export const getRouterRequest = (isLogin) => {
    return (dispatch) => {

        request('POST', '/api/server/web/rest/routerrest/findRouter', {
            cmd: 'findRouter',
            type: 'request',
            request: {
                isLogin
            }
        }).then(obj => {
            console.log(obj, 'routes')
            let res = obj.response;
            if (res.res) {
                message.success('获取路由成功', 1);
                dispatch(getRouterReducer(res.message))
            } else {
                message.error('获取路由失败' + res.exception, 1);
            }
        }).catch(err => console.log(err));
    }
}

const getRouterReducer = (data) => ({
    type: actionType.GET_ROUTER,
    data
})

