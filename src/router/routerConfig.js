import React, { Suspense,lazy } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Loading from '@component/Loading';
import getRouter from './router';

/**
 * @param {pageType} 后台传入的component字符
 * @author zqb
 */
const renderDetail = (pageType) => {
    let dynamicDetail
    try {

        dynamicDetail=lazy(()=>import(`@pages/${pageType}`))

    } catch (err) {
        dynamicDetail = lazy(()=>import('@component/NotFount')).default
        return dynamicDetail;
    }
    return dynamicDetail;
}

const renderRouter = (routers) => {
    if (!Array.isArray(routers)) return null;

    /**
     * 路由遍历，当redirect存在时，创建Redirect标签
     * 若不存在Redirect则创建Route标签, 如存在二到n级路由，则用render属性扩展，递归调用renderRouter函数
     * @author swh
     */

    return (
        <Switch>
            {
                routers.map((route, index) => {
                    if (route.redirect) {
                        return (
                            <Redirect
                                key={route.path || index}
                                exact={route.exact}
                                strict={route.strict}
                                from={route.path}
                                to={route.redirect}
                            />
                        );
                    }

                    return (
                        <Route
                            key={route.key || index}
                            exact={route.exact}
                            strict={route.exact}
                            path={route.path}
                            render={() => {
                                const renderChildRoutes = renderRouter(route.children);
                                if (route.component) {
                                    if (typeof route.component !== 'string') {
                                        return (
                                            <Suspense fallback={<Loading />}>
                                                <route.component route={route}>{renderChildRoutes}</route.component>
                                            </Suspense>
                                        )
                                    } else {
                                        let DynamicDetail = renderDetail(route.component);
                                        return (
                                            <Suspense fallback={<Loading />}>
                                                <DynamicDetail route={route}>{renderChildRoutes}</DynamicDetail>
                                            </Suspense>
                                        )
                                    }
                                }
                                return renderChildRoutes;
                            }}
                        />
                    )
                })
            }
        </Switch>
    )
}

const RouterConfig = ({ routerData }) => {

    return (
        <Router>
            {renderRouter(getRouter(routerData))}
        </Router>
    )
}

const mapStateToProps = (state) => ({
    routerData: state.router.routerData
})

export default connect(mapStateToProps, null)(RouterConfig);