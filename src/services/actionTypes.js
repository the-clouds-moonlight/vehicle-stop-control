/*
 * @Autor: Jiang
 * @Date: 2020-12-25 10:32:02
 * @LastAutor: you name
 * @LastEditTime: 2021-05-18 16:53:45
 * @FilePath: /RemoteExperiment2.0_Web/src/services/actionTypes.js
 */
//  登录相关
export const LOGIN = "login/success";
export const PHONELOGIN = "phonelogin"
export const NOTPHONELOGIN = "notphonelogin"

export const LOGINTEST = "loginTest"
// 忘记密码相关
export const FORGETPASSWORD = "forgetPassword/success";
export const FORGETPASSWORDCODE = "forgetPassword_codeRequest"
export const NOTFORGETPASSWORDCODE = "notforgetPassword_codeRequest"

// 注册相关
export const REGISTER = "register/success";

// 验证码相关
export const VALIDATE = "validate/success";
export const NOTVALIDATE = "notvalidate/success"

export const VALIDATETWO = "validatetwo/success";
export const NOTVALIDATETWO = "notvalidatetwo/success";


export const PHONECODE = "phonecode"
export const NOTPHONECODE = "notphonecode"


//邮箱注册验证码相关
export const EMAILCODE = "emailcode"
export const NOTEMAILCODE = "notemailcode"

//注销登录
export const LOGOUT = "logout/success";

// 获取验证码
export const GET_CODE = "getCode/success"