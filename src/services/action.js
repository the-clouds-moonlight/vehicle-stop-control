/*
 * @Autor: Jiang
 * @Date: 2020-12-25 10:32:03
 * @LastAutor: you name
 * @LastEditTime: 2021-05-18 16:54:11
 * @FilePath: /RemoteExperiment2.0_Web/src/services/action.js
 */
import request,{IP} from '../utils/request.js';
import * as actionTypes from './actionTypes.js';
import { message,Modal} from 'antd';
export default function httpRequest(params) {
      // const IP_URL = process.env.REMOTE_SERVER_IP + params.api
      const IP_URL = "/api/server" + params.api
      return (dispatch) => {
            request("POST", IP_URL, {
                  cmd: params.cmd,
                  type: "request",
                  request: {
                        ...params.request
                  }
            }).then(response => {
                  const result = response.response;
                  // console.log('resultresult', result)
                  if (result.res) {
                        if (params.method === "loginRequest") {
                              // localStorage.setItem("loginStatus","true")
                              // sessionStorage.setItem("roleName",result.roleName || result.message.roleName);
                              sessionStorage.setItem(
                                    "islogin",
                                    JSON.stringify(result.res)
                              );
                              window.location.href = '/BasicLayout/ExcelDemo'
                        //      if(result.roleName === "Student"){
                        //       window.location.href = '/manage/studentsPage/personalCenter'
                        //      } else if( result.roleName === "Teacher" ){
                        //       window.location.href = '/manage/teachersPage/personalCenter'
                        //      } else if ( result.roleName === "Admin" ){
                        //       window.location.href = '/manage/adminPages/personalCenter'
                        //      }
                        } else if( params.method === "phonelogin_codeRequest") {
                              message.success("验证码发送成功", 2); 
                        }else if( params.method === "phone_loginRequest"){
                              if(result.message.writeInformation){
                                    Modal.warning({
                                          title: '该手机号未注册，请您先注册',
                                          onOk() {
                                                window.location.href = '/homePage'
                                          }
                                    })
                              } else {
                                    localStorage.setItem("loginStatus","true")
                                    sessionStorage.setItem("roleName",result.roleName || result.message.roleName);
                                    sessionStorage.setItem(
                                          "islogin",
                                          JSON.stringify(result.res)
                                    );
                                    window.location.href = '/login'
                              }
                        }  else if( params.method ===  "registerRequest"){
                              Modal.success({
                                    title: '账号注册成功，请返回登录',
                                    onOk() {
                                          window.location.href = '/login'
                                    }
                              })
                        } else  if (  params.method === "forgetPasswordRequest"){
                              message.success("密码修改成功，请返回登录", 2); 
                        } else if (  params.method === "phone_codeRequest"){
                              message.success("验证码发送成功", 2); 
                        }else if (  params.method === "email_codeRequest"){
                              message.success("验证码发送成功", 2); 
                        } else if( params.method ===  "forgetPassword_codeRequest"){
                              message.success("验证码发送成功", 2); 
                              dispatch(forgetPassword_codeRequest(result.res))
                              return;
                        }
                        //  else if( params.method ==="loginTestRequest" ){
                        //       message.success("//////", 2); 

                        // }
                        // else {
                              // message.error("注册成功，请返回登录", 1.5);
                        // }
                        dispatch(eval(params.method + "(result)"));
                  } else {
                        message.error(result.exception, 2);
                        if( params.method ===  "forgetPassword_codeRequest"){
                              message.error("验证码发失败", 2); 
                              dispatch(forgetPassword_codeRequest(result.res))
                        }
                  }
            }).catch(error => { console.log(error) });
      };
};
//登录状态查看
export const islogin = (params) => {
      return (dispatch) => {
            request('POST', '/api/server/web/rest/basicUserRest/isLogin', {
                  cmd: 'isLogin',
                  type: 'request',
                  request: {

                  }
            }).then(obj => {
                  // 回复
                  let response = obj.response;
                  // 后台是否异常
                  let res = response.res;
                  // 是否登录
                  
                  if (res) {
                        let flag = response.message.flag;
                        if (flag) {
                              sessionStorage.setItem(
                                    "islogin",
                                    JSON.stringify(flag)
                              );
                        } else {
                              console.log("====>", flag);
                              sessionStorage.setItem(
                                    "islogin",
                                    JSON.stringify(flag)
                              );
                              Modal.warning({
                                    title: '你的账户存在异常，请重新登录！',
                                    onOk() {
                                          window.location.href = '/login'
                                    },
                              });
                              // message.error('获取消息类型失败' + res.exception, 1);
                        }
                  }
                  
            }).catch(err => console.log(err));
      }
};
//注销相关
export const logoutRequest = (params) => {
      return (dispatch) => {
            request('POST', '/api/server/web/rest/basicUserRest/logout', {
                  cmd: 'logout',
                  type: 'request',
                  request: {

                  }
            }).then(response => {
                  const result = response.response;
                  if (result.res) {
                        message.success('账号注销成功', 1);
                        sessionStorage.clear();
                        localStorage.setItem("loginStatus","null")
                        window.location.href = '/login'
                        dispatch(logoutReducer(result.message))
                  } else {
                        message.error('账号注销失败' + result.exception, 1.5);
                  }
            }).catch(error => {
                  console.log(error)
            });
      };
};
//账号校验相关
export const testRequest = (params) => {
      // const IP_URL = process.env.REMOTE_SERVER_IP + params.api
      const IP_URL = "/api/server" + params.api
      // const IP_URL = IP + params.api
      return (dispatch) => {
            request("POST", IP_URL, {
                  cmd: params.cmd,
                  type: "request",
                  request: {
                        ...params.request
                  }
            }).then(response => {
                  const result = response.response;
                  if (result.res) {
                        if (params.method === "loginRequest") {
                              // localStorage.setItem("loginStatus","true")
                              // sessionStorage.setItem("roleName",result.roleName || result.message.roleName);
                              sessionStorage.setItem(
                                    "islogin",
                                    JSON.stringify(result.res)
                              );
                  } else {
                  message.warning('查找失败--' + result.exception, 2);
                  }
                  dispatch(loginTestRequest(result.res))
            }
            }).catch(error => {
                  console.log(error)
            });
      };
};

// 账号注册获取验证码
export const getCode = (params) =>{
      const IP_URL = "/api/server" + params.api
      return(dispatch) =>{
            request("POST", IP_URL,{
                  cmd:params.cmd,
                  type:"request",
                  request:{
                        
                  }
            }).then(response =>{
                  const result = response.response;
                  if(result.res){
                        if(params.method === "validateRequest"){
                              console.log(">>>>>>>>>>"+result.message)
                              dispatch(getCode_(result.message))
                        }
                  }else{
                        message.error("获取失败" +result.exception,3);
                  }
            }).catch(error =>{
                  console.log(error);
            })
      }
}

// 登录相关
const loginRequest = result => ({ type: actionTypes.LOGIN, result });
//登录账号验证
const loginTestRequest = result => ({ type: actionTypes.LOGINTEST, result });
///手机号登录相关
const phonelogin_codeRequest = result => ({ type: actionTypes.PHONELOGIN, result });
export const notphonelogin_codeRequest = result => ({ type: actionTypes.NOTPHONELOGIN, result });

// 忘记密码相关
const forgetPassword_codeRequest = result => ({ type: actionTypes.FORGETPASSWORDCODE, result });
const forgetPasswordRequest = result => ({ type: actionTypes.FORGETPASSWORD, result });

export const notforgetPassword_codeRequest = result => ({ type: actionTypes.NOTFORGETPASSWORDCODE, result });

// 注册相关
const registerRequest = result => ({ type: actionTypes.REGISTER, result });

// 手机验证码相关
const phone_codeRequest = result => ({ type: actionTypes.VALIDATE, result });
export const notphone_codeRequest= result => ({ type: actionTypes.NOTVALIDATE, result }
      // { type: actionTypes.NOTVALIDATETWO, result }
      // { type: actionTypes.NOTPHONECODE, result },
      // { type: actionTypes.NOTEMAILCODE, result }
      );
// 邮箱验证码相关
const email_codeRequest = result => ({ type: actionTypes.VALIDATETWO, result });
export const notemail_codeRequest= result => ({ type: actionTypes.NOTVALIDATETWO, result });
//  获取短信邮箱验证码相关
const phonecode = result => ({ type: actionTypes.PHONECODE, result });
export const notphonecode= result => ({ type: actionTypes.NOTPHONECODE, result });
//邮箱注册验证码相关
const emailcode = result => ({ type: actionTypes.EMAILCODE, result });
export const notemailcode= result => ({ type: actionTypes.NOTEMAILCODE, result });

// //  获取验证码或更新验证码
// export const getValidate = () => {
//       const IP_URL = process.env.REMOTE_SERVER_IP  + params.api
// }
//账号注销
const logoutReducer = (result) => ({
      type: actionTypes.LOGOUT,
      result
})

// 获取验证码
const getCode_ = (result) => ({
      type : actionTypes.VALIDATE,
      result
})