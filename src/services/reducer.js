import * as actionTypes from './actionTypes.js';

export default (state = {}, action) => {
      switch (action.type) {
            case actionTypes.LOGIN: {
                  return {
                        ...state, islogin: true
                  }
            }
            case actionTypes.FORGETPASSWORD: {
                  return {
                        ...state, forgetPasswordStatus: action.res
                  }
            }
            case actionTypes.REGISTER: {
                  return {
                        ...state, registerStatus: action.res
                  }
            }
            case actionTypes.VALIDATE: {
                  return {
                        ...state, validateString: action.result
                  }
            }
            default:
                  return state
      }
}