/*
 * @Author: wuxiangyi
 * @Date: 2022-03-23 14:01:36
 * @LastEditors: wuxiangyi
 * @LastEditTime: 2022-03-24 21:30:19
 * @FilePath: /ReactMiddelware/src/store.js
 * @Description: 
 * 
 */
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';

import Intercept_reducer from '@component/Intercept/store/reducer';
import router_reducer from './router/store/reducer';
import { receiveWebsocketReducer } from '@component/Websocket/reducer';

import Login_reducer from './services/reducer.js';
import { themeReducer } from './layout/theme';

import routerConfig_reducer from '@pages/RouteConfig/store/reducer';
import roleConfig_reducer from '@pages/RoleConfig/store/reducer';
import accountConfig_reducer from '@pages/AccountConfig/store/reducer';
import excelDemo_reducer from '@pages/ExcelDemo/store/reducer';
import MapReducer from './pages/OfflineMap/Map/store/reducer';
import POIReducer from './pages/POIDemo/store/reducer';
import personalFile_reducer from '@pages/Documents/PersonalFile/store/reducer';
import PageQuery_reducer from '@pages/PageQuery/store/reducer';

// import { store_routes } from './router/router'

const win = window;
const middlewares = [thunkMiddleware];

if (process.env.NODE_ENV === 'development') {
    middlewares.push(require('redux-logger').createLogger())
}

const storeEnhancers = compose(
    applyMiddleware(...middlewares),
    (win && win.__REDUX_DEVTOOLS_EXTENSION__) ? win.__REDUX_DEVTOOLS_EXTENSION__() : (f) => f,
);


const reducers = combineReducers({
    websockets: receiveWebsocketReducer,
    Intercept: Intercept_reducer,
    router: router_reducer,
    login: Login_reducer,
    theme: themeReducer,
    routerConfig: routerConfig_reducer,
    roleConfig: roleConfig_reducer,
    accountConfig: accountConfig_reducer,
    excelDemo: excelDemo_reducer,
    mapReducer: MapReducer,
    poiReducer: POIReducer,
    personalFile: personalFile_reducer,
    pageQuery: PageQuery_reducer
})

const initState = {
    websockets: {
        websocketData: {}
    },
    Intercept: {
        visible: true,
        isValidation: true
    },
    router: {
        routerData: [
            // ...store_routes
        ]
    },
    theme: {
        value: 'light'
    },
    routerConfig: {
        data: []
    },
    login: {
        isLogin: JSON.parse(sessionStorage.getItem('islogin')) || false,
        validateString: "abcd", // 默认验证码
        userType: 'admin'
    },
    accountConfig:{
        data:[],
        role:[]
    },
    roleConfig: {
        data: [],
        bindData:[]
    },
    excelDemo: {
        data: {},
        filePath:{}
    },
    mapReducer: {
        currentLatlng: {},
        selectedLatlng: {}
    },
    poiReducer: {
        pdfFilePath: '',
        wordFilePath: ''
    },
    personalFile: {
        data: []
    },
    pageQuery: {
        data: {}
    }
}

export default createStore(reducers, initState, storeEnhancers);