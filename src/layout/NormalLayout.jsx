import React, { Component } from "react";
import Intercept from "@component/Intercept";

class NormalLayout extends Component {
    render() {
        if (sessionStorage.getItem("validation") == 2) {
            return (
                <div style={{ background: "#f0f2f5", minHeight: "100vh" }}>
                    {this.props.children}
                </div>
            );
        } else {
            return (
                <Intercept>
                    <div style={{ background: "#f0f2f5", minHeight: "100vh" }}>
                        {this.props.children}
                    </div>
                </Intercept>
            );
        }
    }
}

export default NormalLayout;
