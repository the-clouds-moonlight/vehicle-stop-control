const CHANGE_THEME = 'basic/changeTheme'

export const changeThemeReducer = (data) => ({
    type: CHANGE_THEME,
    data
})

export const themeReducer = (state = {}, action) => {
    switch (action.type) {
        case CHANGE_THEME: {
            return { ...state, value: action.data }
        }
        default:
            return state;
    }
}