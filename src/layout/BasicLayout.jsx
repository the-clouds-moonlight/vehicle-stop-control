import React, { useState, useEffect } from 'react';
import { Layout, Card, Tabs } from 'antd';
import SiderMenu from '@component/Menu/siderMenu';
import MainHeader from '@component/Menu/headerMenu';
import MainFooter from '@component/Menu/footerMenu';
import CheckLogin from '@component/CheckLogin';

import styles from './style.less';
import { changeThemeReducer } from './theme'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

const { TabPane } = Tabs;

const BasicLayout = ({ route, children, isLogin, history, themeColor, changeTheme }) => {


    const [collapsed, setCollapsed] = useState(false);
    const paneKey = localStorage.getItem('tabsKey');
    const paneTitle = localStorage.getItem('tabsTitle');

    const [panes, setPanes] = useState([])
    const [activeKey, setActiveKey] = useState(undefined)

    /**
     * @param {paneKey} 导航栏key
     * @param {paneTitle} 导航栏title
     * @description 为了防止出现多余的导航栏，将之前的路由路径存储到数组里，如果数组里没有路径，则将其插入到导航栏里
     * @description 这里会有一个依赖缺失的警告，可以不用管，如果加入了panes可以解决警告问题，但会造成无法删除当前导航栏的问题
     */
    useEffect(() => {
        let arr = [];
        panes.forEach(item => arr.push(item.key))
        if (!arr.includes(paneKey) && paneKey) {
            setPanes([...panes, { title: paneTitle, key: paneKey }]);
        }
        setActiveKey(paneKey)
    }, [paneKey, paneTitle])

    const onTabClicks = (key) => {
        history.push(key);
    }

    const remove = (targetKey) => {
        let lastIndex;
        panes.forEach((pane, index) => {
            if (pane.key === targetKey) {
                lastIndex = index - 1;
            }
        })
        const panes2 = panes.filter(pane => pane.key !== targetKey);
        if (panes2.length && activeKey === targetKey) {
            if (lastIndex >= 0) {
                setActiveKey(panes2[lastIndex].key)
                history.push(panes2[lastIndex].key)
            } else {
                setActiveKey(panes2[0].key)
            }
        }
        setPanes(panes2);
    }

    /**
     * 
     * @param {SiderMenu} 路由配置
     * @param {isGetMenu} 是否启用动态路由
     */
    return (
        <CheckLogin isLogin={isLogin} isGetMenu={true}>
            <Layout className={styles.main_layout} >
                <SiderMenu routes={route.children} collapsed={collapsed} onCollapsedChange={(bool) => setCollapsed(bool)} themeColor={themeColor} />
                <Layout className={styles.main_layout_right}>
                    <MainHeader collapsed={collapsed} onCollapsedChange={(bool) => setCollapsed(bool)} themeColor={themeColor} changeTheme={v => changeTheme(v)} />
                    <Layout.Content className={styles.main_layout_content}>
                        <Tabs
                            hideAdd
                            type='editable-card'
                            onChange={key => setActiveKey(key)}
                            onEdit={(key, action) => { if (action === 'remove') { remove(key) } }}
                            onTabClick={onTabClicks}
                            activeKey={activeKey}
                        >
                            {
                                panes.map(i => {
                                    return (
                                        <TabPane tab={i.title} key={i.key} />
                                    )
                                })
                            }
                        </Tabs>
                        <Card>
                            {children}
                        </Card>
                    </Layout.Content >
                    <MainFooter themeColor={themeColor} />
                </Layout>
            </Layout>
        </CheckLogin>
    );
};

const mapStateToProps = state => ({
    isLogin: state.login.isLogin,
    themeColor: state.theme.value
})

const mapDispatchToProps = disptach => ({
    changeTheme(value) {
        disptach(changeThemeReducer(value))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(BasicLayout));