import React from 'react';
import { connect } from 'react-redux';
import { receiveWebsocketData } from './reducer.js';

//  获取本地token 默认为undefined
// const token = localStorage.getItem("x-auth-token") || "undefined";
const token = sessionStorage.getItem("token") || "undefined";
var Socket, lockReconnect = false, WS_IP, websocketStatus = true;

const Websocket = ({ url, setWebsocketData }) => {
      React.useEffect(() => {
            WS_IP = "ws://localhost:8080/wsresult?" + token;
            WS_Connect();
            return () => {
                  try {
                        websocketStatus = false;
                        if (heartCheck.timeoutObj) {
                              clearTimeout(heartCheck.timeoutObj);
                              clearTimeout(heartCheck.serverTimeoutObj)
                              heartCheck.timeoutObj = null;
                              heartCheck.serverTimeoutObj = null;
                        };
                        Socket.close();
                        Socket = null
                  } catch (e) {
                        console.log("e")
                  }
            }
      }, [url]);
      const WS_Connect = () => {
            //  浏览器是否支持websocket
            // if ("WebSocket" in window) {
            try {
                  Socket = new WebSocket(WS_IP);
                  initWS();
            } catch (e) {
                  console.log("创建webSocket失败，重连");
                  // 此处调用重连方法
                  websocketReconnect();
            };

            // };
            // alert("请尝试使用最新版本的Chrome浏览器");
      };
      //  初始化
      const initWS = () => {
            Socket.onopen = function () {
                  heartCheck.reset().start();
            };
            Socket.onmessage = function (info) {
                  var data = JSON.parse(info.data);

                  // 判断是否为心跳
                  if (data.type === "heart") {
                        console.log("websocket心跳数据", data);
                        heartCheck.reset().start();
                  } else {
                        setWebsocketData(JSON.parse(info.data))
                  }

            };
            // 后台服务异常不断调用这两个方法
            Socket.onerror = function (event) {
                  console.log("websocket连接异常", event);
                  if (heartCheck.timeoutObj) {
                        clearTimeout(heartCheck.timeoutObj);
                        clearTimeout(heartCheck.serverTimeoutObj);
                  }
                  websocketReconnect();
            };
            Socket.onclose = function (event) {
                  console.log("websocket连接关闭", event);
                  if (heartCheck.timeoutObj) {
                        clearTimeout(heartCheck.timeoutObj);
                        clearTimeout(heartCheck.serverTimeoutObj);
                  }
                  if (websocketStatus) {
                        websocketReconnect();
                  }
            };
      };
      //  重连方法
      const websocketReconnect = () => {
            if (lockReconnect) return; // 是否已经执行重连
            lockReconnect = true;
            tt && clearTimeout(tt);
            // 没连接上会一直重连，设置延迟避免请求过多
            var tt = setTimeout(function () {
                  // Socket对象置为空
                  Socket = null;

                  console.log("断线重连中，请稍后。。。。。。" + new Date().getTime());
                  WS_Connect();
                  lockReconnect = false;
            }, 3000); // 时间代表为3s中延迟
      };
      // 心跳
      var heartCheck = {
            timeout: 6000,
            timeoutObj: null,
            serverTimeoutObj: null,
            reset: function () {
                  if (this.timeoutObj) {
                        clearTimeout(this.timeoutObj);
                        clearTimeout(this.serverTimeoutObj);
                  }
                  return this;
            },
            start: function () {
                  var self = this;
                  self.timeoutObj = setTimeout(function () {
                        //  心跳数据发送
                        const data = {
                              type: "heart", data: { "message": "心跳" },
                              from: token
                        };
                        try {
                              Socket.send(JSON.stringify(data));
                              //// 判断解决后台突然关闭 Socket关闭报错 WebSocket is already in CLOSING or CLOSED state
                              self.serverTimeoutObj = setTimeout(function () {
                                    //如果onclose会执行websocketReconnect，我们执行Socket.close()就行了.
                                    //如果直接执行websocketReconnect 会触发onclose导致重连两次
                                    Socket.close();
                                    // 前端突然断网 会关闭Socket 连上网络后会进行重连
                                    websocketReconnect()
                              }, self.timeout)
                        } catch (e) {
                              Socket = null;
                              websocketReconnect();
                        }
                  }, self.timeout)
            },
      };
      return null;
};

const mapDispatchToProps = dispatch => ({
      setWebsocketData(data) {
            dispatch(receiveWebsocketData(data))
      }
})
export default connect(null, mapDispatchToProps)(Websocket);