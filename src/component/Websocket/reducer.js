const RECEIVE_WEBSOCKET_DATA = 'receive_websocket_data'

export const receiveWebsocketData = (data) => ({
    type: RECEIVE_WEBSOCKET_DATA,
    data
})

export const receiveWebsocketReducer = (state = {}, action) => {
    switch (action.type) {
        case RECEIVE_WEBSOCKET_DATA: {
            return { ...state, websocketData: action.data }
        }
        default:
            return state;
    }

}