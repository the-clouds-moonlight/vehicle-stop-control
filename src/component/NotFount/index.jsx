import React, { Component } from 'react';
import { Empty, Button } from 'antd';
import { withRouter } from 'react-router-dom';

class NotFount extends Component {
    render() {
        return (
            <Empty
                image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                imageStyle={{
                    height: 60,
                }}
                description={
                    <span>
                        未找到该页面
                    </span>
                }
            >
                <Button type="primary" onClick={this.callback}>返回首页</Button>
            </Empty>
        )
    }

    callback = () => {
        this.props.history.push('/login')
    }
}

export default withRouter(NotFount);