import React, { Component } from 'react';
import styles from './style.less'

class Loading extends Component {
    render() {
        return (
            <div className={styles.loader}>
                <div className={styles.loader_inner}>
                    <div className={styles.loader_line_wrap}>
                        <div className={styles.loader_line}></div>
                    </div>
                    <div className={styles.loader_line_wrap}>
                        <div className={styles.loader_line}></div>
                    </div>
                    <div className={styles.loader_line_wrap}>
                        <div className={styles.loader_line}></div>
                    </div>
                    <div className={styles.loader_line_wrap}>
                        <div className={styles.loader_line}></div>
                    </div>
                    <div className={styles.loader_line_wrap}>
                        <div className={styles.loader_line}></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Loading;