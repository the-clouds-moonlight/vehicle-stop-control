import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Input } from 'antd';
import * as actions from './store/action';
import styles from './index.less';

class index extends Component {
  componentDidMount() {
    this.props.getIsIntercept();
  }

  render() {
    const { visible, isValidation } = this.props;
    // console.log(visible)
    // console.log(isValidation)

    if (isValidation) {
      return this.props.children
    }

    return (
      <div style={{ width: '100%', height: '100vh', background: '#1c2538' }}>
        <p className={styles.p_font}>系统测试，非诚勿扰</p>
        <Modal
          title="口令"
          visible={visible}
          footer={null}
          closable={false}
          bodyStyle={{ background: '#fff' }}
        >
          <Input
            onPressEnter={e => {
              localStorage.setItem('y-auth-token', e.target.value);
              this.props.getIsIntercept();
            }}
          />
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  visible: state.Intercept.visible,
  isValidation: state.Intercept.isValidation
})

const mapDispatchToProps = dispatch => ({
  getIsIntercept: () => {
    dispatch(actions.getIsIntercept());
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(index);


