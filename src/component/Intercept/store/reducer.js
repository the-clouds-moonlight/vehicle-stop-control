import * as actionType from './action'

export default (state = {}, action) => {
  switch (action.type) {
    case actionType.INTERCEPT_VISIBLE: {
      return { ...state, visible: true }
    }
    case actionType.INTERCEPT_ISVALIDATION: {
      console.log(state, 'state store')
      return { ...state, isValidation: true }
    }
    default: return state
  }
}