import request from '@utils/request';

export const INTERCEPT_VISIBLE = 'Intercept/visible';
export const INTERCEPT_ISVALIDATION = 'Intercept/isValidation';

export const getIsIntercept = () => {
  return (dispatch) => {
    const url = '/api/server/web/rest/TestRest/test';
    request('POST', url, {
    }).then(() => {
      switch (sessionStorage.getItem('validation')) {
        case '1':
          return dispatch(set_visible());
        case '2':
          return dispatch(set_isValidation());
        default:
          return null;
      }
    }).catch((erro) => {
      sessionStorage.removeItem('validation');
      sessionStorage.removeItem('y-auth-token');
      console.error(erro)
    })
  }
}

const set_visible = () => ({
  type: INTERCEPT_VISIBLE
})

const set_isValidation = () => ({
  type: INTERCEPT_ISVALIDATION
})