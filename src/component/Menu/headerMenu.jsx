import React from 'react';
import { Layout, Dropdown, Menu, Row, Col, Switch, Space } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import { LoginOutlined, MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import styles from './style.less';
import { connect } from 'react-redux';
import { logoutRequest } from '../../services/action'

/**
 * 右侧头部边栏菜单选项
 * @author swh
 */


const HeaderMenu = ({ collapsed, onCollapsedChange, themeColor, changeTheme, logoutRequest }) => {

    const changeThemeFn = (flag) => {
        if (flag) {
            changeTheme('light')
        } else {
            changeTheme('dark')
        }
    }

    const menu = (
        <Menu>
            <Menu.Item key="0">
                个人信息
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item key="1">
                <p onClick={() => logoutRequest()}>&nbsp; 退出登录</p>
            </Menu.Item>
        </Menu>
    )

    return (
        <div data-theme={themeColor}>
            <Layout.Header className={styles.hfbackground}>
                <Row type="flex" style={{ paddingRight: 20 }}>
                    <Col style={{ flex: 1 }}>
                        {
                            collapsed ? (
                                <MenuUnfoldOutlined onClick={() => onCollapsedChange(!collapsed)} className={styles.icon} />
                            ) : (
                                <MenuFoldOutlined onClick={() => onCollapsedChange(!collapsed)} className={styles.icon} />
                            )
                        }
                        {/* <Icon
                        className="trigger"
                        type={globalStore.collapsed ? 'menu-unfold' : 'menu-fold'}
                        onClick={globalStore.toggleCollapsed}
                    /> */}
                    </Col>
                    <Col style={{ textAlign: 'right' }}>
                        <Space>
                            <Switch checkedChildren="light" unCheckedChildren="dark" defaultChecked onChange={changeThemeFn.bind(this)} size='small' />
                            <Dropdown overlay={menu} trigger={['click', 'hover']} placement='bottomCenter'>
                                <LoginOutlined className={styles.icon} />
                            </Dropdown>
                        </Space>
                    </Col>
                </Row>
            </Layout.Header>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    logoutRequest() {
        dispatch(logoutRequest())
    }
})

export default connect(null, mapDispatchToProps)(HeaderMenu)



