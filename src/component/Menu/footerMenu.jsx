import React from 'react';
import { Layout } from 'antd';
import styles from './style.less'

/**
 * 右侧页脚
 * @author swh 
 */
const MainFooter = (props) => {
    return (
        <div data-theme={props.themeColor}>
            <Layout.Footer className={styles.hfbackground}>
                <div style={{ textAlign: 'center', height: 32 }} className={styles.icon}>
                    &copy;Smartlab411
            </div>
            </Layout.Footer>
        </div>
    )
}

export default MainFooter;