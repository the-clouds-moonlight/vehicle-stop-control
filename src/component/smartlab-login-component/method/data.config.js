import method_RegExp from './resExp.js';

/******************************************************************
      详细配置项： 
            form对象中api对应ant.design中form表单中Form中的配置，可以进行
            对照api进行增加、修改、删除

            formItem对象中api对应ant.design中Form.Item中的配置，可以根据
            需求对相应的api进行增加、修改、删除

      表单api传送门: https://ant.design/components/form-cn/#API
*******************************************************************/

/**
 * @author cc
 * @copyright Smartlab411
 * 
 * @description 忘记密码 邮箱找回
 * @template FORGETPASSWORD_EMAIL
 * 
 * @description 忘记密码 手机号找回
 * @template FORGETPASSWORD_PHONE
 * 
 * @description 邮箱注册
 * @template REGISTER_EMAIL
 * 
 * @description 账号密码注册
 * @template REGISTER_OTHER
 * 
 * @description 手机号注册
 * @template REGISTER_PHONE
 * 
 * @description 邮箱登录
 * @template LOGIN_EMAIL
 * 
 * @description 身份证号登录
 * @template LOGIN_IDNUMBER
 * 
 * @description 账号密码登录
 * @template LOGIN_OTHER
 * 
 * @description 手机号登录
 * @template LOGIN_PHONE
 */

// 忘记密码 邮箱找回
export const FORGETPASSWORD_EMAIL = {
      form: {
            from: {
                  labelCol: { span: 24 },
                  wrapperCol: { span: 24 },
                  name: "basic",
                  initialValues: { remember: true },
                  style: {
                        padding: 24
                  }
            },
            formItem: {
                  // 邮箱表单
                  email: {
                        label: "邮箱",
                        name: "email",
                        rules: [
                              { required: true, message: '输入的邮箱不可为空！' },
                              { pattern: method_RegExp("email"), message: "请输入正确的邮箱" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 邮箱或手机验证码表单
                  reg: {
                        label: "验证码",
                        name: "reg",
                        rules: [
                              { required: true, message: '输入的验证码不可为空' },
                              { pattern: method_RegExp("testCode"), message: "请输入正确的六位数字验证码" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 新密码表单
                  password: {
                        label: "新密码",
                        name: "password",
                        rules: [
                              { required: true, message: '设置的新密码不可为空！' },
                              { pattern: method_RegExp("chinese"), message: "密码设置不可包含汉字" },
                              { min: 6, message: "密码长度最少为6位" },
                              { max: 18, message: "密码长度最多为18位" }
                        ],
                        hasFeedback: true,
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 确认密码表单
                  okPassword: {
                        label: "确认密码",
                        name: "okPassword",
                        rules: [
                              { required: true, message: '请确认您的新密码！' },
                              ({ getFieldValue }) => ({
                                    validator(rule, value) {
                                          if (!value || getFieldValue("password") === value) {
                                                return Promise.resolve();
                                          };
                                          return Promise.reject("新密码和确认密码不相同！")
                                    }
                              })
                        ],
                        hasFeedback: true,
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 人机验证
                  validate: {
                        label: "人机验证",
                        name: "validate",
                        rules: [
                              { required: true, message: "验证码输入不可为空！" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 提交按钮
                  submit: {
                        style: {
                              marginBottom: 0,
                              marginTop: 8
                        },
                        wrapperCol: { offset: 0, span: 24 },
                  }
            }
      }
};

// 忘记密码 手机号找回
export const FORGETPASSWORD_PHONE = {
      form: {
            from: {
                  labelCol: { span: 24 },
                  wrapperCol: { span: 24 },
                  name: "basic",
                  initialValues: { remember: true },
                  style: {
                        padding: 24
                  }
            },
            formItem: {
                  // 手机号表单
                  phone: {
                        label: "手机号",
                        name: "phone",
                        rules: [
                              { required: true, message: '输入的手机号不可为空！' },
                              { pattern: method_RegExp("telephone"), message: "请输入正确的手机号" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 邮箱或手机验证码表单
                  reg: {
                        label: "验证码",
                        name: "reg",
                        rules: [
                              { required: true, message: '输入的验证码不可为空' },
                              { pattern: method_RegExp("testCode"), message: "请输入正确的六位数字验证码" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 新密码表单
                  password: {
                        label: "新密码",
                        name: "password",
                        rules: [
                              { required: true, message: '设置的新密码不可为空！' },
                              { pattern: method_RegExp("chinese"), message: "密码设置不可包含汉字" },
                              { min: 6, message: "密码长度最少为6位" },
                              { max: 18, message: "密码长度最多为18位" }
                        ],
                        hasFeedback: true,
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 确认密码表单
                  okPassword: {
                        label: "确认密码",
                        name: "okPassword",
                        rules: [
                              { required: true, message: '请确认您的新密码！' },
                              ({ getFieldValue }) => ({
                                    validator(rule, value) {
                                          if (!value || getFieldValue("password") === value) {
                                                return Promise.resolve();
                                          };
                                          return Promise.reject("新密码和确认密码不相同！")
                                    }
                              })
                        ],
                        hasFeedback: true,
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 人机验证
                  validate: {
                        label: "人机验证",
                        name: "validate",
                        rules: [
                              { required: true, message: "验证码输入不可为空！" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 提交按钮
                  submit: {
                        style: {
                              marginBottom: 0,
                              marginTop: 8
                        },
                        wrapperCol: { offset: 0, span: 24 },
                  }
            }
      }
};

// 邮箱注册
export const REGISTER_EMAIL = {
      form: {
            from: {
                  labelCol: { span: 24 },
                  wrapperCol: { span: 24 },
                  name: "basic",
                  initialValues: { remember: true },
                  style: {
                        padding: 24
                  }
            },
            formItem: {
                  // 用户名设置表单
                  username: {
                        label: "",
                        name: "username",
                        rules: [
                              { required: true, message: '设置用户名不可为空！' },
                              { pattern: method_RegExp("chinese"), message: "用户名中不可包含汉字" },
                              { min: 6, message: "用户名长度最少为6位" },
                              { max: 18, message: "用户名长度最多为18位" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 设置密码表单
                  password: {
                        // label: "设置密码",
                        name: "password",
                        rules: [
                              { required: true, message: '新密码设置不可为空！' },
                              { pattern: method_RegExp("chinese"), message: "密码中不可包含汉字" },
                              { min: 6, message: "密码长度最少为6位" },
                              { max: 18, message: "密码长度最多为18位" }
                        ],
                        hasFeedback: true,
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 确认密码表单
                  okPassword: {
                        // label: "确认密码",
                        name: "okPassword",
                        rules: [
                              { required: true, message: '请确认您的新密码！' },
                              ({ getFieldValue }) => ({
                                    validator(rule, value) {
                                          if (!value || getFieldValue("password") === value) {
                                                return Promise.resolve();
                                          };
                                          return Promise.reject("新密码和确认密码不相同！")
                                    }
                              })
                        ],
                        hasFeedback: true,
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 邮箱表单
                  email: {
                        // label: "邮箱",
                        name: "email",
                        rules: [
                              { required: true, message: '输入的邮箱不可为空！' },
                              { pattern: method_RegExp("email"), message: "请输入正确的邮箱" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 邮箱或手机验证码表单
                  reg: {
                        // label: "验证码",
                        name: "reg",
                        rules: [
                              { required: true, message: '输入的验证码不可为空' },
                              { pattern: method_RegExp("testCode"), message: "请输入正确的六位数字验证码" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // // 人机验证
                  // validate: {
                  //       // label: "人机验证",
                  //       name: "validate",
                  //       rules: [
                  //             { required: true, message: "验证码输入不可为空！" }
                  //       ],
                  //       style: {
                  //             marginBottom: 0
                  //       }
                  // },
                  // 提交按钮
                  submit: {
                        style: {
                              marginBottom: 0,
                              marginTop: 8
                        },
                        wrapperCol: { offset: 0, span: 24 },
                  }
            }
      }
};

// 手机号注册
export const REGISTER_PHONE = {
      form: {
            from: {
                  labelCol: { span: 24 },
                  wrapperCol: { span: 24 },
                  name: "basic",
                  initialValues: { remember: true },
                  style: {
                        padding: 24
                  }
            },
            formItem: {
                  // 手机号表单
                  phone: {
                        label: "手机号",
                        name: "phone",
                        rules: [
                              { required: true, message: '输入的手机号不可为空！' },
                              { pattern: method_RegExp("telephone"), message: "请输入正确的手机号" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 邮箱或手机验证码表单
                  reg: {
                        label: "验证码",
                        name: "reg",
                        rules: [
                              { required: true, message: '输入的验证码不可为空' },
                              { pattern: method_RegExp("testCode"), message: "请输入正确的六位数字验证码" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 设置密码表单
                  password: {
                        label: "设置密码",
                        name: "password",
                        rules: [
                              { required: true, message: '新密码设置不可为空！' },
                              { pattern: method_RegExp("chinese"), message: "密码中不可包含汉字" },
                              { min: 6, message: "密码长度最少为6位" },
                              { max: 18, message: "密码长度最多为18位" }
                        ],
                        hasFeedback: true,
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 确认密码表单
                  okPassword: {
                        label: "确认密码",
                        name: "okPassword",
                        rules: [
                              { required: true, message: '请确认您的新密码！' },
                              ({ getFieldValue }) => ({
                                    validator(rule, value) {
                                          if (!value || getFieldValue("password") === value) {
                                                return Promise.resolve();
                                          };
                                          return Promise.reject("新密码和确认密码不相同！")
                                    }
                              })
                        ],
                        hasFeedback: true,
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 人机验证
                  validate: {
                        label: "人机验证",
                        name: "validate",
                        rules: [
                              { required: true, message: "验证码输入不可为空！" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 提交按钮
                  submit: {
                        style: {
                              marginBottom: 0,
                              marginTop: 8
                        },
                        wrapperCol: { offset: 0, span: 24 },
                  }
            }
      }
};

// 账号密码注册
export const REGISTER_OTHER = {
      form: {
            from: {
                  labelCol: { span: 24 },
                  wrapperCol: { span: 24 },
                  name: "basic",
                  initialValues: { remember: true },
                  style: {
                        padding: 24
                  }
            },
            formItem: {
                  // 用户名设置表单
                  username: {
                        label: "",
                        name: "username",
                        rules: [
                              { required: true, message: '设置用户名不可为空！' },
                              { pattern: method_RegExp("chinese"), message: "用户名中不可包含汉字" },
                              { min: 6, message: "用户名长度最少为6位" },
                              { max: 18, message: "用户名长度最多为18位" }
                        ],
                        // style: {
                        //       marginBottom: 0
                        // }
                  },
                  // 设置密码表单
                  password: {
                        label: "",
                        name: "password",
                        rules: [
                              { required: true, message: '新密码设置不可为空！' },
                              { pattern: method_RegExp("chinese"), message: "密码中不可包含汉字" },
                              { min: 6, message: "密码长度最少为6位" },
                              { max: 18, message: "密码长度最多为18位" }
                        ],
                        hasFeedback: true,
                        // style: {
                        //       marginBottom: 0
                        // }
                  },
                  // 确认密码表单
                  okPassword: {
                        label: "",
                        name: "okPassword",
                        rules: [
                              { required: true, message: '请确认您的新密码！' },
                              ({ getFieldValue }) => ({
                                    validator(rule, value) {
                                          if (!value || getFieldValue("password") === value) {
                                                return Promise.resolve();
                                          };
                                          return Promise.reject("新密码和确认密码不相同！")
                                    }
                              })
                        ],
                        hasFeedback: true,
                        // style: {
                        //       marginBottom: 0
                        // }
                  },
                  // 人机验证
                  validate: {
                        // label: "人机验证",
                        name: "validate",
                        rules: [
                              { required: true, message: "验证码输入不可为空！" }
                        ],
                        // style: {
                        //       marginBottom: 0
                        // }
                  },
                  // 提交按钮
                  submit: {
                        style: {
                              marginBottom: 0,
                              marginTop: 8
                        },
                        wrapperCol: { offset: 0, span: 24 },
                  }
            }
      }
};

// 邮箱登录
export const LOGIN_EMAIL = {
      form: {
            from: {
                  labelCol: { span: 24 },
                  wrapperCol: { span: 24 },
                  name: "basic",
                  initialValues: { remember: true },
                  style: {
                        padding: 24
                  }
            },
            formItem: {
                  // 邮箱表单
                  email: {
                        label: "邮箱",
                        name: "email",
                        rules: [
                              { required: true, message: '输入的邮箱不可为空！' },
                              { pattern: method_RegExp("email"), message: "请输入正确的邮箱" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 邮箱或手机验证码表单
                  reg: {
                        label: "验证码",
                        name: "reg",
                        rules: [
                              { required: true, message: '输入的验证码不可为空' },
                              { pattern: method_RegExp("testCode"), message: "请输入正确的六位数字验证码" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 人机验证
                  validate: {
                        label: "人机验证",
                        name: "validate",
                        rules: [
                              { required: true, message: "验证码输入不可为空！" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 提交按钮
                  submit: {
                        style: {
                              marginBottom: 0,
                              marginTop: 8
                        },
                        wrapperCol: { offset: 0, span: 24 },
                  }
            }
      }
};

// 身份证号登录
export const LOGIN_IDNUMBER = {
      form: {
            from: {
                  labelCol: { span: 24 },
                  wrapperCol: { span: 24 },
                  name: "basic",
                  initialValues: { remember: true },
                  style: {
                        padding: 24
                  }
            },
            formItem: {
                  // 身份证号表单
                  idNumber: {
                        label: "身份证",
                        name: "idNumber",
                        rules: [
                              { required: true, message: '身份证输入不可为空！' },
                              { pattern: method_RegExp("idNumber"), message: "请输入正确的身份证号" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 密码表单
                  password: {
                        label: "密码",
                        name: "password",
                        rules: [
                              { required: true, message: '密码输入不可为空！' },
                        ],
                        hasFeedback: true,
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 人机验证
                  validate: {
                        label: "人机验证",
                        name: "validate",
                        rules: [
                              { required: true, message: "验证码输入不可为空！" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 提交按钮
                  submit: {
                        style: {
                              marginBottom: 0,
                              marginTop: 8
                        },
                        wrapperCol: { offset: 0, span: 24 },
                  }
            }
      }
};

// 账号密码登录
export const LOGIN_OTHER = {
      form: {
            from: {
                  labelCol: { span: 24 },
                  wrapperCol: { span: 24 },
                  name: "basic",
                  initialValues: { remember: true },
                  style: {
                        padding: 24
                  }
            },
            formItem: {
                  // 用户名表单
                  username: {
                        name: "username",
                        rules: [
                              { required: true, message: '用户名输入不可为空！' }
                        ],
                        // style: {
                        //       marginBottom: 0
                        // }
                  },
                  // 密码表单
                  password: {
                        name: "password",
                        rules: [
                              { required: true, message: '密码输入不可为空！' }
                        ],
                        hasFeedback: true,
                        // style: {
                        //       marginBottom: 0
                        // }
                  },
                  // // 人机验证
                  // validate: {
                  //       label: "人机验证",
                  //       name: "validate",
                  //       rules: [
                  //             { required: true, message: "验证码输入不可为空！" }
                  //       ],
                  //       style: {
                  //             marginBottom: 0
                  //       }
                  // },
                  // 提交按钮
                  submit: {
                        style: {
                              marginBottom: 0,
                              marginTop: 8
                        },
                        wrapperCol: { offset: 0, span: 24 },
                  }
            }
      }
};

// 手机号登录
export const LOGIN_PHONE = {
      form: {
            from: {
                  labelCol: { span: 24 },
                  wrapperCol: { span: 24 },
                  name: "basic",
                  initialValues: { remember: true },
                  style: {
                        padding: 24
                  }
            },
            formItem: {
                  // 手机号表单
                  phone: {
                        label: "手机号",
                        name: "phone",
                        rules: [
                              { required: true, message: '手机号输入不可为空！' },
                              { pattern: method_RegExp("telephone"), message: "请输入正确的手机号" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 邮箱或手机验证码表单
                  reg: {
                        label: "验证码",
                        name: "reg",
                        rules: [
                              { required: true, message: '输入的验证码不可为空' },
                              { pattern: method_RegExp("testCode"), message: "请输入正确的六位数字验证码" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 人机验证
                  validate: {
                        label: "人机验证",
                        name: "validate",
                        rules: [
                              { required: true, message: "验证码输入不可为空！" }
                        ],
                        style: {
                              marginBottom: 0
                        }
                  },
                  // 提交按钮
                  submit: {
                        style: {
                              marginBottom: 0,
                              marginTop: 8
                        },
                        wrapperCol: { offset: 0, span: 24 },
                  }
            }
      }
};