/*
 * @Author: Can-Chen
 * @Date: 2020-06-13 10:58:37
 * @LastEditTime: 2020-06-13 10:59:38
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /test-component/src/smartlab-login-component/regExp/resExp.js
 */ 
export default function (param) {

      if (param.constructor.toString().indexOf("String") < 0) {
            throw new Error("--__ 调用正则传入参数有误 __--");
      };

      const regExp = {

            /* 身份证号 */
            idNumber: /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/,

            /* 中文 */
            chinese: /^[^\u4e00-\u9fa5]{0,}$/,

            /* 电话 */
            telephone: /^(13[0-9]|14[5|7]|15[^4,,\\D]|(17[0|1|3|5-8])|18[0-9])\d{8}$/,

            /* 邮箱 */
            email: /^([a-zA-Z]|[0-9])(\w|-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/,

            /* 邮编 */
            ZipCode: /^[0-8][0-9]\d{4}$/,

            /* 六位验证码 */
            testCode: /^([0-9]{6})$/
      };

      return regExp[param];
}