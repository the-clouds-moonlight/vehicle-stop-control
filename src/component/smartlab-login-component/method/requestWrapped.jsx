import React from 'react';
import store from '../../../store.js';
import Action from '../../../services/action.js';
// import { connect } from 'react-redux';

let RequestLogin = WrappedComponent => class Test extends React.Component {
      // constructor(props) {
      //       super(props);

      //       this.state = {

      //             currentValue: "abcd", // 默认数据
      //       }
      // };
      children = () => {
            let params = {
                  api: "/users/reg",
                  cmd: "reg",
                  request: {},
                  method: "validateRequest"
            };
            store.dispatch(Action(params));
      };
      httpRequest = values => {
            let obj = {
                  api: "/users/login",
                  cmd: "login",
                  request: values,
                  method: "loginRequest"
            };
            store.dispatch(Action(obj))
      };
      // handleChange = e => {
      //       const { currentValue } = this.state;
      //       let prevValue = currentValue;
      //       let _currentValue = this.select(store.getState())

      //       if (prevValue !== _currentValue) {
      //             this.setState({
      //                   currentValue: _currentValue
      //             })
      //       };
      // };

      // select(state) {
      //       return state.login.validateString
      // }
      render() {
            // store.subscribe(this.handleChange)
            const stateProps = {
                  children: this.children,
                  httpRequest: this.httpRequest,
                  // _reg: "abcd"
            };
            return <WrappedComponent {...stateProps} />
      };
};

// const mapStateToProps = state => {
//       return {
//             reg: state.login.validateString
//       }
// };

export default RequestLogin;
