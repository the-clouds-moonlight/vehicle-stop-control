import React, { useState, useEffect } from 'react';
import { Button } from 'antd';

let timer = null;

export const ButtonComponet = (props) => {

      const [status, setStatus] = useState(false),
            [count, setCount] = useState(60);

      const handleClick = () => {
            // 邮箱或手机号验证码请求
            props.methodRequest();
            timerMethod();
      };

      const timerMethod = () => {
            timer = setTimeout(() => {
                  if (count === 1) {
                        clearTimeout(timer);
                        timer = null;
                        setCount(60)
                        setStatus(false)
                        return;
                  }
                  setCount(count - 1);
                  setStatus(true)
            }, 1000);
      }

      useEffect(() => {
            if (count < 60) {
                  timerMethod()
            };
            return () => clearTimeout(timer);
      });
      return (
            <Button
                  disabled={status}
                  onClick={handleClick}
                  style={{ float: "right" }}
            >
                  {
                        !status ?
                              "点击获取验证码" :
                              `${count}s后重新获取`
                  }
            </Button>
      )
}