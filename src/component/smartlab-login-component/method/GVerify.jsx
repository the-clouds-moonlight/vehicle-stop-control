import React, { Component } from 'react';
import { Input, Row, Col } from 'antd';

class GVerify extends Component {

      constructor(props) {
            super(props);
            this.state = {
                  width: "125", //默认canvas宽度
                  height: "40", //默认canvas高度

                  validate: props.validate, // 服务端传入

                  visible: false
            };
      };

      componentDidMount = () => {
            
            this._init();
      };

      // 初始化
      _init = e => {
            
            this.refresh(this.state.validate);
      };

      // 生成验证码
      refresh = e => {
            let canvas = this.refs.verifyCanvas;
            
            if (canvas.getContext) {
                  var ctx = canvas.getContext("2d");
            } else {
                  return;
            };

            ctx.textBaseline = "middle";

            ctx.fillStyle = randomColor(180, 240);
            ctx.fillRect(0, 0, this.state.width, this.state.height);

            // 验证码 服务端传入
            let txt = e;
            for (var i = 1; i <= 4; i++) {
                  // var txt = txtArr[randomNum(0, txtArr.length)];
                  ctx.font = randomNum(this.state.height / 2, this.state.height) + "px SimHei"; // 随机生成字体大小
                  ctx.fillStyle = randomColor(50, 160); // 随机生成字体颜色
                  ctx.shadowOffsetX = randomNum(-3, 3);
                  ctx.shadowOffsetY = randomNum(-3, 3);
                  ctx.shadowBlur = randomNum(-3, 3);
                  ctx.shadowColor = "rgba(0, 0, 0, 0.3)";
                  var x = this.state.width / 5 * i;
                  var y = this.state.height / 2;
                  var deg = randomNum(-30, 30);
                  /**设置旋转角度和坐标原点**/
                  ctx.translate(x, y);
                  ctx.rotate(deg * Math.PI / 180);
                  ctx.fillText(txt[i - 1], 0, 0);
                  /**恢复旋转角度和坐标原点**/
                  ctx.rotate(-deg * Math.PI / 180);
                  ctx.translate(-x, -y);
            };

            // 绘制干扰线
            for (var i = 0; i < 4; i++) {
                  ctx.strokeStyle = randomColor(40, 180);
                  ctx.beginPath();
                  ctx.moveTo(randomNum(0, this.state.width), randomNum(0, this.state.height));
                  ctx.lineTo(randomNum(0, this.state.width), randomNum(0, this.state.height));
                  ctx.stroke();
            };

            // 绘制干扰点
            for (var i = 0; i < this.state.width / 4; i++) {
                  ctx.fillStyle = randomColor(0, 255);
                  ctx.beginPath();
                  ctx.arc(randomNum(0, this.state.width), randomNum(0, this.state.height), 1, 0, 2 * Math.PI);
                  ctx.fill();
            };

            this.setState({ validate: e });
      };

      // 验证码输入框获取焦点
      inputOnFocus = e => {
            // this._init();
            // console.log(e)
      }

      render() {
            const { width, height } = this.state;
            const { onChange } = this.props;

            return (
                  <div style={{ marginTop: 4 }}>
                        <Col>
                              <Row span={24} style={{ marginTop: 12 }}>
                                    <Input
                                          style ={{width:"150px",height:"35px"}}
                                          type="text"
                                          ref="code_input"
                                          onChange={
                                                (e) => onChange(e.target.value)
                                          }
                                          placeholder="请输入验证码"
                                          onFocus={this.inputOnFocus}
                                    />
                             
                                    <div
                                          ref="v_container"
                                          style={{
                                                width: width + "px",
                                                height: height + "px"
                                          }}
                                    >
                                          <canvas
                                                ref="verifyCanvas"
                                                width={this.state.width + "px"}
                                                height={this.state.height + "px"}
                                                style={{ cursor: "pointer",marginLeft:"20px"}}
                                                onClick={this.props.parent}
                                          >您的浏览器版本不支持canvas</canvas>
                                    </div>
                                    </Row>
                        </Col>
                  </div>
            );
      };

      // 更新验证码
      componentDidUpdate() {
            if (
                  this.props.validate !==
                  this.state.validate &&
                  this.props.validate !==
                  undefined
            ) {
                  this.setState({
                        validate: this.props.validate
                  },
                        this._init
                  );
            };
      };
}

export default GVerify;

/**生成一个随机色**/
function randomColor(min, max) {
      var r = randomNum(min, max);
      var g = randomNum(min, max);
      var b = randomNum(min, max);
      return "rgb(" + r + "," + g + "," + b + ")";
};
/**生成一个随机数**/
function randomNum(min, max) {
      return Math.floor(Math.random() * (max - min) + min);
}