/*
 * @Author: your name
 * @Date: 2020-06-14 16:58:54
 * @LastEditTime: 2020-06-14 17:02:33
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /test-component/src/smartlab-login-component/method/md5.js
 */ 
import CryptoJS from 'crypto-js';

// 密码加密
export default function md5_64 (password, username) {

      // 拼接账号密码
      var param = password + username;
  
      // // 判断传入的参数是否包含汉字
      // const pattern = /^[^\u4e00-\u9fa5]{0,}$/;
      // if (!(pattern.test(param))) {
      //     return;
      // };
  
      // 进行MD5加密 不可逆
      var str = CryptoJS.MD5(param).toString(CryptoJS.enc.Hex);
  
      // 进行base64加密 可逆
      var base64_str = CryptoJS.enc.Utf8.parse(str);
      var _str = CryptoJS.enc.Base64.stringify(base64_str);
  
      return _str;
  };
  