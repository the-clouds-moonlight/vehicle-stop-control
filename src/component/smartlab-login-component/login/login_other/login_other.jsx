import React, { Component } from 'react';
import { LOGIN_OTHER } from '../../method/data.config.js';
import GVerify from '../../method/GVerify.jsx';
import MD5 from '../../method/md5.js';
import RequestLogin from '../../method/requestWrapped.jsx';
import { connect } from 'react-redux';
import { Button, Form, Input, message, Radio } from 'antd';
// import { LOGIN_OTHER } from "../../method/data.config.js";
// import GVerify from "../../method/GVerify.jsx";
// import MD5 from "../../method/md5.js";
// import RequestLogin from "../../method/requestWrapped.jsx";
// import { connect } from "react-redux";
import Actions from '../../../../services/action.js';
import { testRequest } from '../../../../services/action.js';
import {
    UserOutlined,
    LockOutlined,
    CheckCircleOutlined,
    CloseCircleOutlined,
  } from "@ant-design/icons";
import styles from '@src/pages/App/style.less';

class login_other extends Component {
    constructor(props) {
        super(props);
        this.state = {
            testnumber: true,
            twotestnumber: false,
        };
    }
    componentDidUpdate(nextProps) {
        if (nextProps.islogintest != this.props.islogintest) {
            if (nextProps.islogintest) {
                this.setState({
                    testnumber: true,
                    twotestnumber: true,
                });
            } else {
                this.setState({
                    testnumber: false,
                    twotestnumber: false,
                });
            }
        }
    }
    // shouldComponentUpdate(prevState, nextProps) {
    //   if (prevState.islogintest != this.state.islogintest) {
    //     return true;
    //   }
    //   return false;
    // }
    render() {
        const form = LOGIN_OTHER.form.from;
        const formItem = LOGIN_OTHER.form.formItem;
        const Keys = Object.keys(formItem);
        let { testnumber, twotestnumber } = this.state;
        // console.log("islogintestislogintest :>> ", this.props.islogintest);
        const method = {
            username: (
                <Input
                    style={{width:'18vw'}}
                    prefix={<UserOutlined style={{ color: 'rgb(0,0,0)'}} />}
                    placeholder="账号"
                    size="large"
                    suffix={
                        <>
                            {testnumber != true ? (
                                <CheckCircleOutlined style={{ color: 'rgb(0,0,0)' }} />
                            ) : null}
                            {twotestnumber == true ? (
                                <CloseCircleOutlined style={{ color: 'rgb(0,0,0)' }} />
                            ) : null}
                        </>
                    }
                    // onBlur={e => this.checkConfirmation(e)}
                />
            ),
            password: (
                <Input.Password
                    style={{width:'18vw'}}
                    className={styles.input_style}
                    type="password"
                    prefix={<LockOutlined style={{ color: 'rgb(0,0,0)' }} />}
                    placeholder="密码"
                    size="large"
                />
            ),
            submit: (
                <Button
                    style={{width:'18vw'}}
                    type="primary"
                    htmlType="submit"
                  //   style={{ width: '100%' }}
                    size="large"
                    className={styles.button_style}
                >
                    登&emsp; &emsp; &emsp; 录
                </Button>
            ),
           
        };
        // console.log(this.props._reg, this.props.reg)
        let { onForgetchange } = this.props;

        return (
            <Form {...form} onFinish={this.onFinish} onFinishFailed={this.onFinishFailed}>
                {Keys.map((item, index) => {
                    return (
                        <Form.Item key={index} {...formItem[item]}>
                            {method[item]}
                        </Form.Item>
                    );
                })}
            </Form>
        );
    }
    ///在输入账号失去焦后与后台校验是否存在与数据库中
    checkConfirmation = e => {
        // console.log("object :>> ", e.target.value);
        let other = {
            userName: e.target.value,
        };
        let obj = {
            api: '/web/rest/user/loginTest',
            cmd: 'loginTest',
            request: other,
            method: 'loginTestRequest',
        };
        this.props.test_Request(obj);
        return;
    };
    //  点击确定验证成功提交表单
    onFinish = values => {
        // console.log("valuesvalues :>> ", values);
        let other = {
            account: values.username,
            password: values.password,
        };
        let obj = {
            api: '/web/rest/basicUserRest/login',
            cmd: 'login',
            request: other,
            method: 'loginRequest',
        };
        this.props.httpRequest(obj);
        return;
    };
    // 点击确定验证失败
    onFinishFailed = errorInfo => {
        message.error('账号或密码错误', 1.5);
    };
}
const mapStateToProps = state => {
    return {
        reg: state.login.validateString,
        islogintest: state.login.islogintest,
    };
};
const mapDispatchToProps = dispatch => ({
    httpRequest: obj => {
        dispatch(Actions(obj));
    },
});
export default RequestLogin(connect(mapStateToProps, mapDispatchToProps)(login_other));
