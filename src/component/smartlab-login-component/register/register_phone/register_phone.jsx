import React, { Component } from 'react';
import { Row, Col, Button, Form, Input, message } from 'antd';
import { ButtonComponet } from '../../method/button.jsx';
import { REGISTER_PHONE } from '../../method/data.config.js';
import Actions from '../../../../services/action.js';
import GVerify from '../../method/GVerify.jsx';
import MD5 from '../../method/md5.js';
import { connect } from 'react-redux';
class register_phone extends Component {
      render() {
            const form = REGISTER_PHONE.form.from;
            const formItem = REGISTER_PHONE.form.formItem;
            const Keys = Object.keys(formItem);
            const method = {
                  phone: <Input />,
                  reg: (
                        <Row gutter={12}>
                              <Col span={12}>
                                    <Input />
                              </Col>
                              <Col span={12}>
                                    <ButtonComponet methodRequest={this.request} />
                              </Col>
                        </Row>
                  ),
                  password: <Input.Password />,
                  okPassword: <Input.Password />,
                  validate: <GVerify parent={this.children} validate={this.props.reg} />,
                  submit: <Button type="primary" htmlType="submit">点击注册</Button>
            }
            return (
                  <Form
                        {...form}
                        onFinish={this.onFinish}
                        onFinishFailed={this.onFinishFailed}
                  >
                        {Keys.map((item, index) => {
                              return (
                                    <Form.Item
                                          key={index}
                                          {...formItem[item]}
                                    >
                                          {method[item]}
                                    </Form.Item>
                              )
                        })}
                  </Form>
            );
      };
      // 接收来自子组件button的请求验证码
      request = () => {
            let params = {
                  api: "/users/phone_email",
                  cmd: "phone_email",
                  request: {},
                  method: "phone_emailRequest"
            };
            this.props.httpRequest(params);
      }
      // 获取验证码
      componentDidMount() { this.children() }
      // 重新获取验证码
      children = () => {
            let params = {
                  api: "/users/reg",
                  cmd: "reg",
                  request: {},
                  method: "validateRequest"
            }
            this.props.httpRequest(params);
      };
      //  点击确定验证成功提交表单
      onFinish = values => {
            if (
                  values.validate.toLowerCase() ===
                  this.props.reg.toLowerCase()
            ) {
                  values.password
                        = MD5(values.password, values.username);

                  let obj = {
                        api: "/users/login",
                        cmd: "login",
                        request: values,
                        method: "loginRequest"
                  };
                  this.props.httpRequest(obj);
                  return;
            };

            this.children();
            message.error("验证码输入有误！", 2);
      };
      // 点击确定验证失败
      onFinishFailed = errorInfo => {
            message.error("输入的信息不符合规范", 1.5);
      };
}
const mapDispatchToProps = dispatch => ({
      httpRequest: (obj) => {
            dispatch(Actions(obj))
      },
});

const mapStateToProps = state => ({
      reg: state.login.validateString
});

export default connect(mapStateToProps, mapDispatchToProps)(register_phone);