import React, { Component } from 'react';
import { Button, Form, Input, message } from 'antd';
import { REGISTER_OTHER } from '../../method/data.config.js';
import Actions from '../../../../services/action.js';
import { getCode } from '../../../../services/action.js';
import GVerify from '../../method/GVerify.jsx';
import MD5 from '../../method/md5.js';
import { connect } from 'react-redux';
import {
      UserOutlined,
      LockOutlined,
      CheckCircleOutlined,
      CloseCircleOutlined,
} from "@ant-design/icons";
import styles from '@src/pages/App/style.less';

class register_other extends Component {
      constructor(props) {
            super(props);
            this.state = {
                  testnumber: true,
                  twotestnumber: false,
            };
      }
      componentDidUpdate(nextProps) {
            if (nextProps.islogintest != this.props.islogintest) {
                  if (nextProps.islogintest) {
                        this.setState({
                              testnumber: true,
                              twotestnumber: true,
                        });
                  } else {
                        this.setState({
                              testnumber: false,
                              twotestnumber: false,
                        });
                  }
            }
      }
      render() {
            const form = REGISTER_OTHER.form.from;
            const formItem = REGISTER_OTHER.form.formItem;
            const Keys = Object.keys(formItem);
            let { testnumber, twotestnumber } = this.state;
            const method = {
                  username: <Input
                        style={{ width: '18vw' }}
                        prefix={<UserOutlined style={{ color: 'rgb(0,0,0)' }} />}
                        placeholder="账号"
                        size="large"
                        suffix={
                              <>
                                    {testnumber != true ? (
                                          <CheckCircleOutlined style={{ color: 'rgb(0,0,0)' }} />
                                    ) : null}
                                    {twotestnumber == true ? (
                                          <CloseCircleOutlined style={{ color: 'rgb(0,0,0)' }} />
                                    ) : null}
                              </>
                        }
                  // onBlur={e => this.checkConfirmation(e)}
                  />,
                  password: (
                        <Input.Password
                              style={{ width: '18vw'}}
                              className={styles.input_style}
                              type="password"
                              prefix={<LockOutlined style={{ color: 'rgb(0,0,0)' }} />}
                              placeholder="密码"
                              size="large"
                        />
                  ),
                  okPassword: <Input.Password
                        style={{ width: '18vw'}}
                        className={styles.input_style}
                        type="password"
                        prefix={<LockOutlined style={{ color: 'rgb(0,0,0)' }} />}
                        placeholder="再次输入密码"
                        size="large"
                  />,
                  validate: <GVerify style={{width:'18vw'}} parent={this.children} validate={this.props.reg} />,
                  submit: <Button type="primary" style={{width:'18vw'}} htmlType="submit">点击注册</Button>
            }
            return (
                  <Form
                        {...form}
                        onFinish={this.onFinish}
                        onFinishFailed={this.onFinishFailed}
                  >
                        {Keys.map((item, index) => {
                              return (
                                    <Form.Item
                                          key={index}
                                          {...formItem[item]}
                                    >
                                          {method[item]}
                                    </Form.Item>
                              )
                        })}
                  </Form>
            );
      };
      // 获取验证码
      componentDidMount() { this.children() }
      // 重新获取验证码
      children = () => {
            let params = {
                  api: "/web/rest/basicUserRest/getCode",
                  cmd: "getCode",
                  request: {},
                  method: "validateRequest"
            }
            this.props.getCode(params);
      };
      //  点击确定验证成功提交表单
      onFinish = values => {
            if (
                  values.validate.toLowerCase() ===
                  this.props.reg.toLowerCase()
            ) {
                  // values.password
                  //       = MD5(values.password, values.username);

                  let obj = {
                        api: "/web/rest/basicUserRest/addBasicUser",
                        cmd: "addBasicUser",
                        request: values,
                        method: "registerRequest"
                  };
                  this.props.httpRequest(obj);
                  return;
            };

            this.children();
            message.error("验证码输入有误！", 2);
      };
      // 点击确定验证失败
      onFinishFailed = errorInfo => {
            message.error("输入的信息不符合规范", 1.5);
      };
}
const mapDispatchToProps = dispatch => ({
      httpRequest: (obj) => {
            dispatch(Actions(obj))
      },
      getCode: (obj) => {
            dispatch(getCode(obj))
      }
});

const mapStateToProps = state => ({
      reg: state.login.validateString
});

export default connect(mapStateToProps, mapDispatchToProps)(register_other);