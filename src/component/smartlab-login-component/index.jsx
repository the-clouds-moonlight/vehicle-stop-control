// 账号密码登录
export { default as LoginOther } from './login/login_other/login_other.jsx';

// 手机号登录
export { default as LoginPhone } from './login/login_phone/login_phone.jsx';

// 邮箱登录
export { default as LoginEmail } from './login/login_email/login_email.jsx';

// 身份证号登录
export { default as LoginIdNumber } from './login/login_idNumber/login_idNumber.jsx';

//  账号密码注册
export { default as RegisterOther } from './register/register_other/register_other.jsx';

// 手机号注册
export { default as RegisterPhone } from './register/register_phone/register_phone.jsx';

// 邮箱注册
export { default as RegisterEmail } from './register/register_email/register_email.jsx';

// 手机号找回密码
export { default as ForgetPasswordPhone } from './forgetPassword/forgetPassword_phone/forgetPassword_phone.jsx';

// 邮箱找回密码
export { default as ForgetPasswordEmail } from './forgetPassword/forgetPassword_email/forgetPassword_email.jsx';


/**
 * 
 * npm install antd crypto-js --save
 * 
 */