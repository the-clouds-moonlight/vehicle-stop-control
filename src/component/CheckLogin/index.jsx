import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import action from '../../services/action.js';
import { getRouterRequest } from '@router/store/action';
import { islogin } from '../../services/action'

class CheckLogin extends React.Component {
    componentDidMount() {
        // const obj = { method: "loginRequest", api: "/users/islogin", cmd: "islogin", request: {} };
        // this.props.httpRequest(obj);
        if (!this.props.islogin) {
            window.location.href = '/login'
        }
        this.props.getMenu();
        // this.props.isLogin();

    }

    componentDidUpdate() {
        this.props.isLogin();
        if (!this.props.isLogin) {
            this.props.history.push('/login');
        }
       
    }

    render() {
        return this.props.children;
    }


}

const mapStateToPorps = (state) => ({
    islogin: state.login.isLogin
})

const mapDispatchToProps = dispatch => {
    return {
        httpRequest(obj) {
            dispatch(action(obj))
        },
        getMenu() {
            dispatch(getRouterRequest(true))
        },
        isLogin() {
            dispatch(islogin())
        }
    }
}
export default connect(mapStateToPorps, mapDispatchToProps)(withRouter(CheckLogin));