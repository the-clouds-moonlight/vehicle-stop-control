/**
 * @param {remoteServerIP} 和后台调试用的后台ip，如需部署远程也需要改动这里
 * @param {clientIP} 本地前端客户端使用的ip
 * @author swh
 */

const remoteServerIP = 'http://127.0.0.1:8080';
const clientIP = 'http://127.0.0.1:3000';

module.exports = {
    remoteServerIP,
    clientIP
}