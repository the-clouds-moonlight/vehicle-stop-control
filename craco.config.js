const CracoLessPlugin = require('craco-less');
const path = require('path');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const chalk = require('chalk');
const Webpack = require('webpack');
const TerserWebpackPlugin = require('terser-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const commonIP = require('./commonIP');
const proxy = require('./proxy');

module.exports = {

    webpack: {
        //路径映射 如 @src 代表 /src文件夹
        alias: {
            "@src": path.resolve("src"),
            "@pages": path.resolve('src/pages'),
            "@component": path.resolve('src/component'),
            "@utils": path.resolve('src/utils'),
            "@assets": path.resolve('src/assets'),
            "@layout": path.resolve('src/layout'),
            "@router": path.resolve('src/router')
        },
        plugins: [
            //显示启动和打包进度条
            new HardSourceWebpackPlugin(),
            new ProgressBarPlugin({
                format: `٩(๑❛ᴗ❛๑)۶ build  [:bar]  ${chalk.green.bold('(:percent)')}  ${chalk.red.bold('(:elapsed 秒)')}`,
                complete: chalk.green.bold('-'),
                clear: false
            }),
            //设置全局变量，必须要加双引号
            new Webpack.DefinePlugin({
                'process.env': {
                    REMOTE_SERVER_IP: `"${commonIP.remoteServerIP}"`,
                    CLIENT_IP: `"${commonIP.clientIP}"`,
                }
            }),

            // new BundleAnalyzerPlugin({
            //     analyzerMode: 'server',
            //     //  将在“服务器”模式下使用的主机启动HTTP服务器。
            //     analyzerHost: '127.0.0.1',
            //     //  将在“服务器”模式下使用的端口启动HTTP服务器。
            //     analyzerPort: 8888,
            //     //  路径捆绑，将在`static`模式下生成的报告文件。
            //     //  相对于捆绑输出目录。
            //     reportFilename: 'report.html',
            //     //  模块大小默认显示在报告中。
            //     //  应该是`stat`，`parsed`或者`gzip`中的一个。
            //     //  有关更多信息，请参见“定义”一节。
            //     defaultSizes: 'parsed',
            //     //  在默认浏览器中自动打开报告
            //     openAnalyzer: true,
            //     //  如果为true，则Webpack Stats JSON文件将在bundle输出目录中生成
            //     generateStatsFile: false,
            //     //  如果`generateStatsFile`为`true`，将会生成Webpack Stats JSON文件的名字。
            //     //  相对于捆绑输出目录。
            //     statsFilename: 'stats.json',
            //     //  stats.toJson（）方法的选项。
            //     //  例如，您可以使用`source：false`选项排除统计文件中模块的来源。
            //     //  在这里查看更多选项：https：  //github.com/webpack/webpack/blob/webpack-1/lib/Stats.js#L21
            //     statsOptions: null,
            //     logLevel: 'info' // 日志级别。可以是'信息'，'警告'，'错误'或'沉默'。
            // })

        ],
        // configure: (webpackConfig, { env, paths }) => {

        //     webpackConfig.optimization = {
        //         minimizer: [
        //             new TerserWebpackPlugin({
        //                 parallel: true
        //             })
        //         ],
        //         splitChunks: {
        //             chunks: 'async', // 三选一： "initial" | "all" | "async" (默认)
        //             minSize: 20000, // 最小尺寸，30K，development 下是10k，越大那么单个文件越大，chunk 数就会变少（针对于提取公共 chunk 的时候，不管再大也不会把动态加载的模块合并到初始化模块中）当这个值很大的时候就不会做公共部分的抽取了
        //             maxSize: 0, // 文件的最大尺寸，0为不限制，优先级：maxInitialRequest/maxAsyncRequests < maxSize < minSize
        //             minChunks: 1, // 默认1，被提取的一个模块至少需要在几个 chunk 中被引用，这个值越大，抽取出来的文件就越小
        //             maxAsyncRequests: 5, // 在做一次按需加载的时候最多有多少个异步请求，为 1 的时候就不会抽取公共 chunk 了
        //             maxInitialRequests: 3, // 针对一个 entry 做初始化模块分隔的时候的最大文件数，优先级高于 cacheGroup，所以为 1 的时候就不会抽取 initial common 了
        //             automaticNameDelimiter: '~', // 打包文件名分隔符
        //             name: true, // 拆分出来文件的名字，默认为 true，表示自动生成文件名，如果设置为固定的字符串那么所有的 chunk 都会被合并成一个
        //             cacheGroups: {
        //                 // vendors: {
        //                 //     test: /[\\/]node_modules[\\/]/, // 正则规则，如果符合就提取 chunk
        //                 //     priority: -10 // 缓存组优先级，当一个模块可能属于多个 chunkGroup，这里是优先级
        //                 // },
        //                 // default: {
        //                 //     minChunks: 2,
        //                 //     priority: -20, // 优先级
        //                 //     reuseExistingChunk: true // 如果该chunk包含的modules都已经另一个被分割的chunk中存在，那么直接引用已存在的chunk，不会再重新产生一个
        //                 // }
        //                 vendors: {
        //                     chunks: 'all',
        //                     test: /(react|react-dom|react-router|react-router-dom|redux|react-redux)/,
        //                     priority: 100,
        //                     name: 'vendors',
        //                 },
        //                 antdCommons: {
        //                     chunks: 'async',
        //                     test: /antd/,
        //                     priority: 100,
        //                     name: 'antdCommons'
        //                 },
        //                 echartsVendors: {
        //                     chunks: 'async',
        //                     name: 'echartsVendors',
        //                     priority: 100,
        //                     test: /(echarts|echart-for-react)/,
        //                 },
        //                 asyncCommons: {
        //                     chunks: 'async',
        //                     minChunks: 2,
        //                     name: 'async-commons',
        //                     priority: 90,
        //                 },
        //                 commons: {
        //                     chunks: 'all',
        //                     minChunks: 2,
        //                     name: 'commons',
        //                     priority: 80
        //                 }
        //             }
        //         }
        //     }


        //     console.log(webpackConfig.optimization.splitChunks.cacheGroups, 'config')
        //     return webpackConfig;
        // }
    },
    plugins: [
        //支持antd的less样式
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: { '@primary-color': '#1DA57A' },
                        javascriptEnabled: true,
                    },
                },
                //加入css modules后 css名字将会转变成hash唯一值
                cssLoaderOptions: {
                    modules: { localIdentName: '[local][name]_[hash:base64:5]' }
                }
            },
        },
    ],
    //反向代理
    devServer: {
        host: '0.0.0.0', //启动主机号，默认是localhost,设置 0.0.0.0 服务器外部可访问
        port: 3002, //启动端口，默认是3000
        historyApiFallback: true, //解决开启页面后白屏问题
        proxy: {
            ...proxy  //在proxy.js文件修改实际代理
        },
    }
};